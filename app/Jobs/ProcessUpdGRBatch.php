<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use App\Models\Receipt;
use App\Models\ReceiptBatch;
use Log;

class ProcessUpdGRBatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $receipts = null;
    protected $receiptItems = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Receipt $receipts, $receiptItems)
    {
        $this->receipts = $receipts;
        $this->receiptItems = $receiptItems;
    }

    /**
     * Execute the job.
     * Check when all batch is status completed then change status good receipt to completed receipt
     * @return void
     */
    public function handle()
    {   
        $receiptDb = $this->receipts;
        $checkTransaction = Receipt::getReceiptSuggestDelivery($receiptDb->id);

        $statusid = '2';
        $countQtyGap = 0;

        foreach($checkTransaction as $r) {
            if($r->gap_qty == 0) $countQtyGap++; 
        }

        if($countQtyGap == sizeof($checkTransaction)) {
            $statusid = '3';
        }

        $updStatusReceipt = Receipt::updateStatus($statusid, $receiptDb->id);

        $logid = null;
        if (empty($logid)) $logid = uniqid();
        $unique = $logid;
        $sendParams = [
            'InfoTransaction' => $checkTransaction,
            'param' => [
                'statusid' => $statusid,
                'receiptid' => $receiptDb->id
            ],
            'response' => $updStatusReceipt
        ];

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Run Queue : ProcessUpdGRBatch : ".json_encode($sendParams)."\n";
        $user = get_current_user();
        $f = fopen(storage_path().'/logs/apps/'.$date.'.'.$user.'_good_receipt_warung'.'.log','a');
        fwrite($f,$msg);
        fclose($f);
    }
}
