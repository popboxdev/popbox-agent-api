<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SepulsaTransaction extends Model
{
    protected $connection= 'popbox_db';
    protected $table = 'tb_sepulsa_transaction';

    public function detail(){
        return $this->hasMany(DetailSepulsaTransaction::class,'transaction_id','transaction_id');
    }
}
