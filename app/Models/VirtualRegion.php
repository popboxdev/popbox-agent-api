<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualRegion extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'regions';
}
