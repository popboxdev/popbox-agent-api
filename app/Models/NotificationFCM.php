<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NotificationFCM extends Model
{
    protected $table = 'notification_fcm';

    /**
     * Insert new notification
     * @param null $module
     * @param mixed $to Array or String
     * @param string $device
     * @param string $title
     * @param string $body
     * @param array $data
     * @return \stdClass
     */
    public function insertNew($module=null,$to,$device='android',$title='',$body='',$data=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        if (!is_array($to)){
            $to = [$to];
        }

        foreach ($to as $item) {
            // insert to DB
            $dataDb = new self();
            $dataDb->module = $module;
            $dataDb->to = $item;
            $dataDb->device = $device;
            $dataDb->title = $title;
            $dataDb->body = $body;
            $dataDb->data = json_encode($data);
            $dataDb->status = 'created';
            $dataDb->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Transaction Notification
     * @param $transactionId
     * @param array $fcmToken
     * @param $transactionData Transaction get Transaction
     * @param $title
     * @param string $status
     * @param $module
     * @return \stdClass
     */
    public static function createTransactionNotification($transactionId,$fcmToken=[],$transactionData,$title='',$status='process',$module='',$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $transactionItem = $transactionData->items;
        $description='';
        $description.="<b>No. Trx</b>: $transactionData->reference <br>";
        $transactionStatus = $transactionData->status;
        if ($transactionData->status == 'PAID') $transactionStatus = 'Terbayar';
        if ($transactionData->status == 'REFUND') $transactionStatus = 'Refund';
        $description.="<b>Status</b>: $transactionStatus<br>";
        if (!empty($remarks)){
            $description .= "$remarks<br><br>";
        }
        foreach ($transactionItem as $item){
            if ($item->type == 'pulsa'){
                $param = json_decode($item->params);
                $phone = $param->phone;
                $operator = strtoupper($param->operator);
                $price = number_format($item->price);
                $description.="<b>No. Handphone</b>: $phone <br>";
                $description.="<b>Produk</b>: $item->name <br>";
                $description.="<b>Operator</b>: $operator <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'electricity' || $item->type == 'electricity_postpaid'){
                $param = json_decode($item->params);
                $meterNumber = $param->meter_number;
                $productName = $item->name;
                $price = number_format($item->price);
                $description.="<b>No. Pelanggan</b>: $meterNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'admin_fee'){
                $price = number_format($item->price);
                $description.="<br><br>";
                $description.="<b>Produk</b>: Biaya Administrasi <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'bpjs_kesehatan'){
                $param = json_decode($item->params);
                $bpjsNumber = $param->bpjs_number;
                $productName = 'BPJS Kesehatan';
                $price = number_format($item->price);
                $description.="<b>No. BPJS</b>: $bpjsNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'pdam'){
                $param = json_decode($item->params);
                $meterNumber = $param->pdam_number;
                $operator = strtoupper($param->operator_code);
                $operator = str_replace('_',' ',$operator);
                $price = number_format($item->price);
                $description.="<b>No. Pelanggan</b>: $meterNumber <br>";
                $description.="<b>Produk</b>: PDAM <br>";
                $description.="<b>Operator</b>: $operator <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'telkom_postpaid'){
                $param = json_decode($item->params);
                $telkomNumber = $param->telkom_number;
                $productName = 'Telkom';
                $price = number_format($item->price);
                $description.="<b>No. Telkom</b>: $telkomNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            }
        }

        $to = $fcmToken;
        $device='android';
        $body=$transactionData->description;

        $imageUrl = '';
        if ($status == 'process') $imageUrl = url('img/transaction/trx-onprocess.png');
        elseif ($status == 'success') $imageUrl = url('img/transaction/trx-success.png');
        elseif ($status == 'failed') $imageUrl = url('img/transaction/trx-failed.png');

        $fcmData = [];
        $fcmData['id'] = (int)"1".$transactionId;
        $fcmData['type'] = 'transaction';
        $fcmData['timestamp'] = date('Y-m-d H:i:s');
        $fcmData['description'] = $description;
        $fcmData['reference'] = $transactionData->reference;
        $fcmData['img_url'] = $imageUrl;
        $fcmData['locker_id'] = $transactionData->locker_id;

        // insert into fcm
        $notificationFCM =  new NotificationFCM();
        $insert = $notificationFCM->insertNew($module,$to,$device,$title,$body,$fcmData);

        $response->isSuccess = true;
        return $response;
    }
}
