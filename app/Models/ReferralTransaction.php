<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ReferralTransaction extends Model
{
    protected $table = 'referral_transactions';

    /**
     * Create Referral Transaction
     * @param $referralCode
     * @param $campaignId
     * @param $lockerIdTo
     * @return \stdClass
     */
    public static function createTransaction($referralCode,$campaignId,$lockerIdTo){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get campaignDB
        $campaignDb = ReferralCampaign::find($campaignId);
        if (!$campaignDb){
            $response->errorMsg = 'Campaign Referral Not Found';
            return $response;
        }
        $lockerIdForm = null;
        // find referral on locker
        $lockerDb = Locker::where('referral_code',$referralCode)->first();
        if ($lockerDb){
            $lockerIdForm = $lockerDb->id;
        }
        // add second checking for referral code based on code and agent id
        if (empty($lockerIdForm)){
            $lockerIdForm = $campaignDb->from_locker_id;
        }

        $amountDayExpired = $campaignDb->expired;

        // insert into referral transaction
        $transactionDb = new self();
        $transactionDb->referral_campaign_id = $campaignId;
        $transactionDb->code = $referralCode;
        $transactionDb->from_locker_id = $lockerIdForm;
        $transactionDb->to_locker_id = $lockerIdTo;
        $transactionDb->type = $campaignDb->type;
        $transactionDb->from_amount = $campaignDb->from_amount;
        $transactionDb->to_amount = $campaignDb->to_amount;
        $transactionDb->status = 'PENDING';
        $transactionDb->submit_date = date('Y-m-d H:i:s');
        $transactionDb->used_date = null;
        $transactionDb->expired_date = date('Y-m-d H:i:s',strtotime("+$amountDayExpired days"));
        $transactionDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Check Rule Pass
     * @param $toLockerId
     * @param $type
     * @return \stdClass
     */
    public static function checkRulePass($toLockerId,$type,$rule=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = 'General Error Failed';
        $response->isFailed = false;

        if ($type == 'topup'){
            // check if agent has topup transaction
            $userDb = User::where('locker_id',$toLockerId)->get();
            $userId = [];
            foreach ($userDb as $user){
                $userId[] = $user->id;
            }
            // get topup Transaction
            $transactionTopUpDb = Transaction::whereIn('users_id',$userId)
                ->where('type','topup')
                ->where('status','PAID')
                ->orderBy('id','asc')
                ->first();
            if (!$transactionTopUpDb) {
                $tmp = json_encode($userId);
                $response->errorMsg = "First TopUp $tmp not Found";
            }

            if (!empty($rule) && !empty($transactionTopUpDb)){
                $topUpAmount = $transactionTopUpDb->total_price;
                $schemaTransactionRule = json_decode($rule);
                $transactionValue = reset($schemaTransactionRule);
                $transactionOperator = key($schemaTransactionRule);
                if ($transactionOperator == 'between'){
                    $transactionValue = explode('-',$transactionValue);
                    #if price between the transaction rule
                    if ($topUpAmount >= $transactionValue[0] && $topUpAmount <= $transactionValue[1]){
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule Between '.$topUpAmount;
                        $response->isFailed = true;
                    }
                } elseif ($transactionOperator == '>'){
                    if ($topUpAmount > $transactionValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule > '.$topUpAmount;
                        $response->isFailed = true;
                    }
                } elseif ($transactionOperator == '>='){
                    if ($topUpAmount >= $transactionValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule >= '.$topUpAmount;
                        $response->isFailed = true;
                    }
                } elseif ($transactionOperator == '<'){
                    if ($topUpAmount < $transactionValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule < '.$topUpAmount;
                        $response->isFailed = true;
                    }
                } elseif ($transactionOperator == '<='){
                    if ($topUpAmount <= $transactionValue) {
                        $response->isSuccess = true;
                    } else {
                        $response->errorMsg = 'Failed Top Up Rule <= '.$topUpAmount;
                        $response->isFailed = true;
                    }
                }
            } else {
                $response->errorMsg = "First TopUp and Rule not Found";
            }
        } elseif ($type == 'register'){
            // get member destination
            $userDb = User::where('locker_id',$toLockerId)->where('status','<>','0')->first();
            if ($userDb){
                $response->isSuccess = true;
            } else {
                $response->errorMsg = 'Member Not Found';
            }
        } else {
            $response->errorMsg = 'Invalid Type';
        }

        return $response;
    }

    /**
     * Process Referral
     * @param null $fromLockerId
     * @param $toLockerId
     * @param int $fromAmount
     * @param int $toAmount
     * @param $code
     * @param $campaignId
     * @return \stdClass
     */
    public static function processReferral($fromLockerId=null,$toLockerId,$fromAmount=0,$toAmount=0,$code,$campaignId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $location = storage_path()."/logs/cron/";

        $toLockerDb = Locker::find($toLockerId);
        $fromLockerDb = null;
        $fromTransactionId = null;

        // create transaction for from locker
        Log::logFile($location,'referralTransaction',"Process From Locker $fromLockerId");
        if (!empty($fromLockerId)){
            $fromLockerDb = Locker::find($fromLockerId);
            // get user DB
            $userDb = User::where('locker_id',$fromLockerId)->first();
            if ($userDb){
                $username = $userDb->username;
                $remarks = "Referral From $toLockerDb->locker_name";
                // insert into pending transaction
                $reference = 'TOP-'.$userDb->id.date('ymdh').Helper::generateRandomString(3);
                $description = "Top Up $remarks ".number_format($fromAmount)." at ".date('Y-m-d H:i:s');
                $pendingTransaction = PendingTransaction::createPendingTransaction($username,'referral',$reference,$description,$fromAmount);
                if (!$pendingTransaction->isSuccess){
                    $response->errorMsg = "Failed Create Pending Transaction $pendingTransaction->errorMsg";
                    Log::logFile($location,'referralTransaction',"Failed Create Pending Transaction $pendingTransaction->errorMsg");
                    return $response;
                }
                $fromTransactionId = $pendingTransaction->transactionId;
                Log::logFile($location,'referralTransaction',"Success Create Pending Transaction $reference");
            } else {
                Log::logFile($location,'referralTransaction',"User From Locker Not Exist");
            }
        } else Log::logFile($location,'referralTransaction',"$fromLockerId Not Exist");

        // create transaction for to locker
        // get user DB
        $userDb = User::where('locker_id',$toLockerId)->first();
        Log::logFile($location,'referralTransaction',"Process To Locker $toLockerId");
        if ($userDb){
            $username = $userDb->username;
            $remarks = "Referral Code $code";
            if (!empty($fromLockerDb)) $remarks = "Referral Code $code from $fromLockerDb->locker_name";

            // insert into pending transaction
            $reference = 'TOP-'.$userDb->id.date('ymdh').Helper::generateRandomString(3);
            $description = "Top Up $remarks ".number_format($toAmount)." at ".date('Y-m-d H:i:s');
            $pendingTransaction = PendingTransaction::createPendingTransaction($username,'referral',$reference,$description,$toAmount,$fromTransactionId);
            if (!$pendingTransaction->isSuccess){
                $response->errorMsg = "Failed Create Pending Transaction $pendingTransaction->errorMsg";
                Log::logFile($location,'referralTransaction',"Failed Create Pending Transaction $pendingTransaction->errorMsg");
                return $response;
            }
            Log::logFile($location,'referralTransaction',"Success Create Pending Transaction $reference");
        } else {
            Log::logFile($location,'referralTransaction',"User To Locker Not Exist");
        }

        // update referral transaction
        $referralDb = self::find($campaignId);
        $referralDb->used_date = date('Y-m-d H:i:s');
        $referralDb->status = 'USED';
        $referralDb->save();
        $response->isSuccess = true;
        Log::logFile($location,'referralTransaction',"Finish Process");
        return $response;
    }

    /*relationship*/
    public function referral(){
        return $this->belongsTo(ReferralCampaign::class,'campaign_referral_id','id');
    }
}
