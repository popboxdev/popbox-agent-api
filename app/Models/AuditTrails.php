<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditTrails extends Model
{
    protected $table = 'audit_trails';
    protected $primaryKey = 'id';
}
