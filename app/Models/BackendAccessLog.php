<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackendAccessLog extends Model
{
    // set table
    protected $table = 'backend_access_log';
    protected $primaryKey = 'id';
    
    public static function store($params) {
        
        $data = new self();
        $data->module = $params["module"];
        $data->type = $params["type"];
        $data->email = $params["email"];
        $data->name = $params["name"];
        $data->phone = $params["phone"];
        $data->request_json = json_encode($params["request_json"]);
        $data->response_json = json_encode($params["response_json"]);
        $data->created_at = date('Y-m-d H:i:s');
        $data->save();
        
        return $data;
    }
}
