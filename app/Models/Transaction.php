<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Virtual\Locker;

class Transaction extends Model
{
    use SoftDeletes;
    // set table
    protected $table = 'transactions';
    // set date attributes
    protected $dates = ['created_at','updated_at','deleted_at'];

    /*============== Public Function ==============*/
    /**
     * Create Transaction and Items
     * @param $username
     * @param $cartDb
     * @return \stdClass
     */
    public static function createTransactionByCart($username,$cartDb){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionRef = null;

        // get userDb
        $userDb = User::with(['virtualLocker'])
            ->where('username',$username)
            ->first();

        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }
        // get cart Data
        $cartId = $cartDb->cartId;
        $items = $cartDb->data->items;
        $userType = $userDb->virtualLocker->type; 

        //$check = self::checkIndomarco($cartId); #replaced by checkPopShop
        $check = self::checkPopShop($cartId, $userType);
        if (!$check->isSuccess){
            $response->errorMsg = $check->errorMsg;
            return $response;
        }

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TRX-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'purchase';
        $transactionDb->cart_id = $cartId;
        $transactionDb->description = "Transaction from ".$cartDb->data->cart_reference." at ".date('Y-m-d H:i:s');
        // create total price
        $totalPrice = 0;
        foreach ($items as $item){
            $totalPrice += $item->price;
        }
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'UNPAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create Transaction form Cart ".$cartDb->data->cart_reference;
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'UNPAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        return $response;
    }

    /**
     * Create Single Transaction
     * @param $username
     * @param $itemName
     * @param $itemType
     * @param $itemPrice
     * @param array $itemParams
     * @param null $itemPicture
     * @return \stdClass
     */
    public static function createSingleTransaction($username,$itemName,$itemType,$itemPrice,$itemParams=[],$itemPicture = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $transactionType = 'purchase';
        if ($itemType == 'delivery') $transactionType = 'delivery';
        if ($itemType == 'electricity') $transactionType = 'payment';
        if ($itemType == 'electricity_postpaid') $transactionType = 'payment';
        if ($itemType == 'pdam') $transactionType = 'payment';
        if ($itemType == 'bpjs_kesehatan') $transactionType = 'payment';
        if ($itemType == 'telkom_postpaid') $transactionType = 'payment';

        if ($itemType == 'pulsa'){
            // check pulsa transaction
            $check = self::checkPulsa($itemParams);
            if (!$check->isSuccess){
                $response->errorMsg = $check->errorMsg;
                return $response;
            }
        }

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TRX-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = $transactionType;
        $transactionDb->cart_id = null;
        $transactionDb->description = "Transaction $itemName at ".date('Y-m-d H:i:s');

        if ($itemType=='pulsa'){
            // get operator
            if (!empty($itemParams['operator'])){
                $itemPicture = asset('img/logo_operator')."/".$itemParams['operator'].".png";
            }
        }
        // change price popshop to total price
        if ($itemType == 'popshop'){
            $amount = $itemParams['amount'];
            $itemPrice = $itemPrice * $amount;
        }
        // create total price
        $totalPrice = $itemPrice;
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'UNPAID';
        $transactionDb->save();

        if (empty($itemParams['agent_name'])){
            $agentName = $userDb->locker->locker_name;
            $itemParams['agent_name'] = $agentName;
        }

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        $items = [];
        $item = new \stdClass();
        $item->type = $itemType;
        $item->price = $totalPrice;
        $item->name = $itemName;
        $item->picture = $itemPicture;
        $item->params = json_encode($itemParams);
        $items[] = $item;

        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create Single Transaction $itemName";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'UNPAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        return $response;
    }

    /**
     * Create Single Transaction With Multiple Items
     * @param $username
     * @param $itemName
     * @param $itemType
     * @param $itemTotalPrice
     * @param array $items
     * @return \stdClass
     */
    public static function createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $transactionType = 'purchase';
        if ($itemType == 'delivery') $transactionType = 'delivery';
        if ($itemType == 'electricity') $transactionType = 'payment';
        if ($itemType == 'electricity_postpaid') $transactionType = 'payment';
        if ($itemType == 'bpjs_kesehatan') $transactionType = 'payment';
        if ($itemType == 'pdam') $transactionType = 'payment';
        if ($itemType == 'telkom_postpaid') $transactionType = 'payment';
        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TRX-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = $transactionType;
        $transactionDb->cart_id = null;
        $transactionDb->description = "Transaction $itemName at ".date('Y-m-d H:i:s');

        if ($itemType=='pulsa'){
            // get operator
            if (!empty($itemParams['operator'])){
                $itemPicture = asset('img/logo_operator')."/".$itemParams['operator'].".png";
            }
        }
        // change price popshop to total price
        if ($itemType == 'popshop'){
            $response->errorMsg = 'Invalid Single Transaction Type';
        }
        $totalPrice = 0;
        foreach ($items as $item){
            $totalPrice += $item->price;
        }
        // create total price
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'UNPAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create Transaction $itemName";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'UNPAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        return $response;
    }

    /**
     * get single transaction
     * @param $transactionRef
     * @return \stdClass
     */
    public static function getTransaction($transactionRef,$username){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $userDb = User::where('username',$username)->first();

        // get transaction DB
        $transactionDb = self::where('reference',$transactionRef)->where('users_id',$userDb->id)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction Number';
            return $response;
        }
        $newVAList = ['bni_va','doku_mandiri_va','doku_permata_va','doku_alfamart_va'];
        $dokuVAList = [];

        // create response data
        $data = new \stdClass();
        $data->agent = $transactionDb->user->name;
        $data->locker = $transactionDb->user->locker->locker_name;
        $data->locker_id = $transactionDb->user->locker_id;
        $data->reference = $transactionDb->reference;
        $data->description = $transactionDb->description;
        $data->total_price = $transactionDb->total_price;
        $data->status = strtoupper($transactionDb->status);
        $data->type = $transactionDb->type;
        $data->payment = null;
        $tmpPaymentDetail = new \stdClass();
        $tmpPaymentDetail->destination_bank = null;
        $tmpPaymentDetail->destination_account = null;
        $tmpPaymentDetail->sender_bank = null;
        $tmpPaymentDetail->sender_name = null;
        $tmpPaymentDetail->time_limit = null;
        $data->payment_detail = $tmpPaymentDetail;
        if ($transactionDb->payment){
            $data->payment = $transactionDb->payment->method->name;
            // get payment method detail
            $method = $transactionDb->payment->method->code;
            if ($method=='transfer'){
                // get payment transfer db
                $transferDb = PaymentTransfer::where('payments_id',$transactionDb->payment->id)->first();
                $tmp = new \stdClass();
                $tmp->destination_bank = $transferDb->destination_bank;
                $tmp->destination_account = $transferDb->destination_account;
                $tmp->alfamart_va = null;
                $tmp->sender_bank = $transferDb->sender_bank;
                $tmp->sender_name = $transferDb->sender_name;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $data->payment_detail = $tmp;
            } elseif ($method=='doku_va'){
                $dokuVA = PaymentDokuVA::where('payments_id',$transactionDb->payment->id)->first();
                $tmp = new \stdClass();
                $permataVA = $dokuVA->permata_va;
                if (empty($permataVA)) $permataVA = $dokuVA->va_number;
                $tmp->destination_bank = 'PERMATA';
                $tmp->destination_account = $permataVA;
                $tmp->alfamart_va = $dokuVA->alfamart_va;
                $tmp->sender_bank = null;
                $tmp->sender_name = null;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $data->payment_detail = $tmp;
            } elseif ($method=='otto_qr') {
                $paymentNonVA = PaymentNonVA::where('id_payment',$transactionDb->payment->id)->first();
                $tmp = new \stdClass();
                $tmp->destination_bank = null;
                $tmp->destination_account = null;
                $tmp->sender_bank = null;
                $tmp->sender_name = null;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $tmp->status = $paymentNonVA->status;
                $tmp->qr_code = $paymentNonVA->qr_code;
                $tmp->payment_id = $paymentNonVA->payment_id;
                $data->payment_detail = $tmp;
            } elseif (in_array($method,$dokuVAList)){
                $destinationBank = 'PERMATA';
                if ((strpos($method, 'mandiri') !== false)) $destinationBank = 'MANDIRI';
                if ((strpos($method, 'permata') !== false)) $destinationBank = 'PERMATA';
                if ((strpos($method, 'alfamart') !== false)) $destinationBank = 'ALFAMART';

                $dokuVA = PaymentDokuVA::where('payments_id',$transactionDb->payment->id)->first();
                $virtualNumber = $dokuVA->va_number;
                if (empty($virtualNumber) && $destinationBank == 'PERMATA') $virtualNumber = $dokuVA->permata_va;
                $tmp = new \stdClass();
                $tmp->destination_bank = $destinationBank;
                $tmp->destination_account = $virtualNumber;
                $tmp->sender_bank = null;
                $tmp->sender_name = null;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $data->payment_detail = $tmp;
            } elseif (in_array($method,$newVAList)){
                $destinationBank = 'BNI';
                if ((strpos($method, 'bni') !== false)) $destinationBank = 'BNI';
                if ((strpos($method, 'mandiri') !== false)) $destinationBank = 'MANDIRI';
                if ((strpos($method, 'permata') !== false)) $destinationBank = 'PERMATA';
                if ((strpos($method, 'alfamart') !== false)) $destinationBank = 'ALFAMART';

                $dokuVA = PaymentVirtualAccount::where('payments_id',$transactionDb->payment->id)->first();
                $virtualNumber = $dokuVA->va_number;
                $tmp = new \stdClass();
                $tmp->destination_bank = $destinationBank;
                $tmp->destination_account = $virtualNumber;
                $tmp->sender_bank = null;
                $tmp->sender_name = null;
                $tmp->time_limit = $transactionDb->payment->time_limit;
                $data->payment_detail = $tmp;
            }
        }
        $data->total_commission = 0;
        $data->created_at = date('Y-m-d H:i:s',strtotime($transactionDb->created_at));
        $data->updated_at = date('Y-m-d H:i:s',strtotime($transactionDb->updated_at));
        // get items
        $items = [];
        $transactionItems = $transactionDb->items;
        $commissionTotal = 0;
        foreach ($transactionItems as $transactionItem) {
            $item = new \stdClass();
            $item->name = $transactionItem->name;
            $item->type = $transactionItem->type;
            $item->price = $transactionItem->price;
            $item->commission = 0;
            $item->picture = $transactionItem->picture;
            $item->params = $transactionItem->params;
            $item->serial_number = $transactionItem->serial_number;
            $item->details = !empty($transactionItem->reference) ? self::getSepulsaTransactions($transactionItem->reference) : '';

            $commissionId = $transactionItem->commission_schemas_id;
            $basicPrice = $transactionItem->price;
            $publishPrice = $transactionItem->price;
            $schemaDb = CommissionSchema::calculateById($commissionId,$basicPrice,$publishPrice);
            if ($schemaDb->isSuccess){
                $item->commission = (int)$schemaDb->commission;
            }
            $commissionTotal += $schemaDb->commission;

            $items[] = $item;
        }
        $histories = [];
        $transactionHistories = $transactionDb->histories;
        foreach ($transactionHistories as $transactionHistory) {
            $history = new \stdClass();
            $history->user = $transactionHistory->user;
            $history->status = $transactionHistory->status;
            $history->remarks = $transactionHistory->remarks;
            $history->created_at = date('Y-m-d H:i:s',strtotime($transactionHistory->created_at));
            $histories[] = $history;
        }
        $data->items = $items;
        $data->histories = $histories;
        $data->total_commission = (int)$commissionTotal;
        // response
        $response->isSuccess = true;
        $response->data = $data;
        return $response;
    }

    /**
     * remove transaction
     * @param $transactionRef
     * @param $username
     * @return \stdClass
     */
    public static function removeTransaction($transactionRef,$username){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transDB = self::where('reference',$transactionRef)->first()->user()->where('username',$username)->first();
        if (!$transDB){
            $response->errorMsg = 'Invalid Transaction Reference';
            return $response;
        }

        if ($transDB->status=='PAID'){
            $response->errorMsg = 'Cannot Remove Paid Transaction';
            return $response;
        }

        $transDB = self::where('reference',$transactionRef)->delete();

        $userDb = User::where('username',$username)->first();
        // insert to history
        $transactionId = $transDB->id;
        $user = $userDb->name;
        $remarks = "Remove Transaction $transactionRef";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'REMOVED',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * update status transaction
     * @param $transactionId
     * @param $status
     * @return \stdClass
     */
    public static function updateStatusTransaction($transactionId,$status){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $statusList = ['UNPAID','WAITING','PAID','CANCELED'];
        $status = strtoupper($status);
        if (!in_array($status,$statusList)){
            $response->errorMsg = 'Invalid Status';
            return $response;
        }

        // update to DB
        $data = self::find($transactionId);
        $data->status = $status;
        $data->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * list transaction with pagination
     * @param $username
     * @return \stdClass
     */
    public static function listTransaction($username,$input=[],$page=1){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->total_data = 0;
        $response->pages = [];
        
        // get user
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }
        
//         $locker = Locker::getLockerByLockerId($userDb->locker_id);
        
        $type = empty($input['type']) ? null : $input['type'];
        $status = empty($input['status']) ?  null :  $input['status'];
        $startDate = empty($input['start_date']) ? ''  : date('Y-m-d 00:00:00',strtotime($input['start_date'])) ;
        $endDate = empty($input['end_date']) ? '' : date('Y-m-d 23:59:59',strtotime($input['end_date']));
        
//         if(!(strtoupper($locker->type) == strtoupper('warung'))){
//             $startDate = empty($input['start_date']) ? date('Y-m-d 00:00:00',strtotime('-29 days'))  : date('Y-m-d 00:00:00',strtotime($input['start_date'])) ;
//             $endDate = empty($input['end_date']) ? date('Y-m-d 23:59:59') : date('Y-m-d 23:59:59',strtotime($input['end_date']));
//         }
        
        // get transaction list
        $sql = self::where('users_id',$userDb->id)
        ->when($type,function ($query) use ($type){
            if (is_array($type)) return $query->whereIn('type',$type);
            return $query->where('type',$type);
        })->when($status,function ($query) use ($status){
            if (is_array($status)) return $query->whereIn('status',$status);
            return $query->where('status',$status);
        });
            
        if(!empty($startDate) && !empty($endDate)){
            $sql->whereBetween('created_at',[$startDate,$endDate]);
        }
        
        $transactionDb = $sql->orderBy('created_at','desc')->get();

        $transactionList = [];
        $openVAList = ['bni_va','doku_bca_va'];
        $dokuVAList = ['doku_mandiri_va','doku_permata_va','doku_alfamart_va'];
        foreach ($transactionDb as $item) {
            // create response data
            $data = new \stdClass();
            $data->agent = $item->user->name;
            $data->locker = $item->user->locker->locker_name;
            $data->locker_id = $item->user->locker_id;
            $data->reference = $item->reference;
            $data->description = $item->description;
            $data->total_price = $item->total_price;
            $data->status = strtoupper($item->status);
            $data->type = $item->type;
            $data->payment = null;

            $tmpPaymentDetail = new \stdClass();
            $tmpPaymentDetail->destination_bank = null;
            $tmpPaymentDetail->destination_account = null;
            $tmpPaymentDetail->sender_bank = null;
            $tmpPaymentDetail->sender_name = null;
            $tmpPaymentDetail->time_limit = null;
            $data->payment_detail = $tmpPaymentDetail;

            if ($item->payment){
                $data->payment = $item->payment->method->name;
                // get payment method detail
                $method = $item->payment->method->code;
                if ($method=='transfer'){
                    // get payment transfer db
                    $transferDb = PaymentTransfer::where('payments_id',$item->payment->id)->first();
                    $tmp = new \stdClass();
                    $tmp->destination_bank = $transferDb->destination_bank;
                    $tmp->destination_account = $transferDb->destination_account;
                    $tmp->sender_bank = $transferDb->sender_bank;
                    $tmp->sender_name = $transferDb->sender_name;
                    $tmp->time_limit = $item->payment->time_limit;
                    $data->payment_detail = $tmp;
                }
                elseif ($method=='doku_va'){
                    $dokuVA = PaymentDokuVA::where('payments_id',$item->payment->id)->first();
                    $permataVA = $dokuVA->permata_va;
                    if (empty($permataVA)) $permataVA = $dokuVA->va_number;
                    $tmp = new \stdClass();
                    $tmp->destination_bank = 'PERMATA';
                    $tmp->destination_account = $permataVA;
                    $tmp->sender_bank = null;
                    $tmp->sender_name = null;
                    $tmp->time_limit = $item->payment->time_limit;
                    $data->payment_detail = $tmp;
                }
                elseif (in_array($method,$dokuVAList)){
                    $destinationBank = 'PERMATA';
                    if ((strpos($method, 'mandiri') !== false)) $destinationBank = 'MANDIRI';
                    if ((strpos($method, 'permata') !== false)) $destinationBank = 'PERMATA';
                    if ((strpos($method, 'alfamart') !== false)) $destinationBank = 'ALFAMART';
                    if ((strpos($method, 'bca') !== false)) $destinationBank = 'BCA';

                    $dokuVA = PaymentDokuVA::where('payments_id',$item->payment->id)->first();
                    if ($dokuVA){
                        $virtualNumber = $dokuVA->va_number;
                        if (empty($virtualNumber) && $destinationBank == 'PERMATA') $virtualNumber = $dokuVA->permata_va;
                    } else {
                        $dokuVA = PaymentVirtualAccount::where('payments_id',$item->payment->id)->first();
                        $virtualNumber = $dokuVA->va_number;
                    }

                    $tmp = new \stdClass();
                    $tmp->destination_bank = $destinationBank;
                    $tmp->destination_account = $virtualNumber;
                    $tmp->sender_bank = null;
                    $tmp->sender_name = null;
                    $tmp->time_limit = $item->payment->time_limit;
                    $data->payment_detail = $tmp;
                }
                elseif (in_array($method,$openVAList)){
                    $destinationBank = 'BNI';
                    if ((strpos($method, 'bni') !== false)) $destinationBank = 'BNI';
                    if ((strpos($method,'bca') !== false)) $destinationBank = 'BCA';

                    $vaOpen = LockerVAOpen::where('lockers_id',$userDb->locker_id)->where('va_type',$method)->first();
                    if ($vaOpen) {
                        $virtualNumber = $vaOpen->va_number;
                        $tmp = new \stdClass();
                        $tmp->destination_bank = $destinationBank;
                        $tmp->destination_account = $virtualNumber;
                        $tmp->sender_bank = null;
                        $tmp->sender_name = null;
                        $tmp->time_limit = null;
                        $data->payment_detail = $tmp;
                    }
                }
            }
            $data->created_at = date('Y-m-d H:i:s',strtotime($item->created_at));
            $data->updated_at = date('Y-m-d H:i:s',strtotime($item->updated_at));
            $totalCommission = 0;
            // get items
            $items = [];
            $transactionItems = $item->items;
            foreach ($transactionItems as $transactionItem) {
                $item = new \stdClass();
                $item->name = $transactionItem->name;
                $item->type = $transactionItem->type;
                $item->price = $transactionItem->price;
                $item->commission = 0;
                $item->picture = $transactionItem->picture;
                $item->params = $transactionItem->params;
                $item->serial_number = $transactionItem->serial_number;
                $item->details = !empty($transactionItem->reference) ? self::getSepulsaTransactions($transactionItem->reference) : '';

                $commissionId = $transactionItem->commission_schemas_id;
                $basicPrice = $transactionItem->price;
                $publishPrice = $transactionItem->price;
                $commission = CommissionSchema::calculateById($commissionId,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $item->commission = (int)$commission->commission;
                    $totalCommission += $commission->commission;
                }
                $items[] = $item;
            }
            $data->total_commission = (int)$totalCommission;
            $data->items = $items;
            $transactionList[] = $data;
        }

        $transactionList = collect($transactionList);
        $totalData = count($transactionList);
        $pageList = $transactionList->chunk(10);
        $pages = [];
        foreach ($pageList as $key => $value){
            $pages[] = $key+1;
        }
        $chunk = $transactionList->forPage($page,10);
        $transactionList = $chunk->values()->all();

        $response->total_data = $totalData;
        $response->pages =  $pages;

        $response->isSuccess = true;
        $response->data = $transactionList;
        return $response;
    }

    public static function getSepulsaTransactions($reference)
    {
        // $response = new \stdClass();
        $response = '';
        $data = SepulsaTransaction::where('invoice_id', $reference)->first();
        
        if (!empty($data)) {
            $type = $data->product_type;
            $res = json_decode($data->response_data);
            $params = [];
            switch ($type) {
                case 'telkom_postpaid':
                    if ($res) {
                        $params = [
                            "nama_customer" => isset($res->nama_pelanggan) ? $res->nama_pelanggan : '',
                            "periode" => isset($res->bulan_thn) ? $res->bulan_thn : '',
                            "rincian_tagihan" => [],
                            "denda" => 0,
                        ];
                    }
                    break;

                case 'electricity_postpaid':
                    $penalty = 0;
                    $bill = [];

                    if (isset($res->bills)) {
                        foreach ($res->bills as $key => $value) {
                            $penalty += $value->penalty_fee[0];
                            $bill[] = [
                                "bill_periode" => $value->bill_period[0],
                                "bill_amount" => $value->total_electricity_bill[0],
                                "penalty" => $value->penalty_fee[0]
                            ];
                        }                 
                        $params = [
                            "nama_customer" => $res->subscriber_name,
                            "periode" => $res->blth_summary,
                            "rincian_tagihan" => $bill,
                            "denda" => $penalty
                        ];
                    }
                    break;

                case 'bpjs_kesehatan':
                    if (isset($res->data)) {
                        $params = [
                            "nama_customer" => !empty($res->data->name) ? $res->data->name : '',
                            "periode" => !empty($res->data->periode) ? $res->data->periode : '',
                            "rincian" => [
                                "no_va" => !empty($res->data->no_va) ? $res->data->no_va : '',
                                "no_va_kk" => !empty($res->data->no_va_kk) ? $res->data->no_va_kk : '',
                                "va_count" => !empty($res->data->va_count) ? $res->data->va_count : '',
                            ],
                            "denda" => 0,
                        ];
                    }                 
                    break;

                case 'pdam':
                    $date = [];
                    $bills = [];
                    $penalty = 0;
                    if (isset($res->data)) {
                        foreach ($res->data->bills as $key => $value) {
                            $date[] = [$value->bill_date[0]];
                            $penalty += $value->penalty[0];
                            $bills[] = [
                                "bill_periode" => $value->bill_date[0],
                                "bill_amount" => $value->bill_amount[0],
                                "penalty" => $value->penalty[0],
                            ];
                        }
                        $params = [
                            "nama_customer" => $res->data->name,
                            "periode" => $date,
                            "rincian_bulanan" => $bills,
                            "denda" => $penalty,
                        ];
                    }                 
                    break;
                
                default:
                    $params = [
                        "nama_customer" => "",
                        "periode" => [],
                        "rincian_bulanan" => [],
                        "denda" => 0,
                    ];
                    break;
            }
            $response = $params;
        }
        return $response;
    }
    /**
     * List All Transaction without Pagination
     * @param $username
     * @param array $input
     * @param int $page
     * @return \stdClass
     */
    public static function listAllTransaction($username,$input=[],$page=1){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->total_data = 0;
        $response->pages = [];

        // get user
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'Invalid User';
            return $response;
        }

        $type = empty($input['type']) ? null : $input['type'];
        $status = empty($input['status']) ?  null :  $input['status'];
        $startDate = empty($input['start_date']) ? date('Y-m-d H:i:s',strtotime('-30 days'))  : date('Y-m-d H:i:s',strtotime($input['start_date'])) ;
        $endDate = empty($input['end_date']) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime($input['end_date']));


        // get transaction list
        $transactionDb = self::where('users_id',$userDb->id)
            ->when($type,function ($query) use ($type){
                if (is_array($type)) return $query->whereIn('type',$type);
                return $query->where('type',$type);
            })->when($status,function ($query) use ($status){
                if (is_array($status)) return $query->whereIn('status',$status);
                return $query->where('status',$status);
            })->whereBetween('created_at',[$startDate,$endDate])
            ->orderBy('updated_at','desc')
            ->get();
        $transactionList = [];
        foreach ($transactionDb as $item) {
            // create response data
            $data = new \stdClass();
            $data->agent = $item->user->name;
            $data->locker = $item->user->locker->locker_name;
            $data->reference = $item->reference;
            $data->description = $item->description;
            $data->total_price = $item->total_price;
            $data->status = strtoupper($item->status);
            $data->type = $item->type;
            $data->payment = null;
            $data->payment_detail = null;
            if ($item->payment){
                $data->payment = $item->payment->method->name;
                // get payment method detail
                $method = $item->payment->method->code;
                if ($method=='transfer'){
                    // get payment transfer db
                    $transferDb = PaymentTransfer::where('payments_id',$item->payment->id)->first();
                    $tmp = new \stdClass();
                    $tmp->destination_bank = $transferDb->destination_bank;
                    $tmp->destination_account = $transferDb->destination_account;
                    $tmp->alfamart_va = null;
                    $tmp->sender_bank = $transferDb->sender_bank;
                    $tmp->sender_name = $transferDb->sender_name;
                    $tmp->time_limit = $item->payment->time_limit;
                    $data->payment_detail = $tmp;
                } elseif ($method=='doku_va'){
                    $dokuVA = PaymentDokuVA::where('payments_id',$item->payment->id)->first();
                    $tmp = new \stdClass();
                    $tmp->destination_bank = 'PERMATA';
                    $tmp->destination_account = $dokuVA->permata_va;
                    $tmp->alfamart_va = $dokuVA->alfamart_va;
                    $tmp->sender_bank = null;
                    $tmp->sender_name = null;
                    $tmp->time_limit = $item->payment->time_limit;
                    $data->payment_detail = $tmp;
                }
            }
            $data->created_at = date('Y-m-d H:i:s',strtotime($item->created_at));
            $data->updated_at = date('Y-m-d H:i:s',strtotime($item->updated_at));
            $totalCommission = 0;
            // get items
            $items = [];
            $transactionItems = $item->items;
            foreach ($transactionItems as $transactionItem) {
                $item = new \stdClass();
                $item->name = $transactionItem->name;
                $item->type = $transactionItem->type;
                $item->price = $transactionItem->price;
                $item->commission = 0;
                $item->picture = $transactionItem->picture;
                $item->params = $transactionItem->params;
                $item->serial_number = $transactionItem->serial_number;

                $commissionId = $transactionItem->commission_schemas_id;
                $basicPrice = $transactionItem->price;
                $publishPrice = $transactionItem->price;
                $commission = CommissionSchema::calculateById($commissionId,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $item->commission = (int)$commission->commission;
                    $totalCommission += $commission->commission;
                }
                $items[] = $item;
            }
            $data->total_commission = (int)$totalCommission;
            $data->items = $items;
            $transactionList[] = $data;
        }

        $transactionList = collect($transactionList);

        $response->isSuccess = true;
        $response->data = $transactionList;
        return $response;
    }

    /**
     * Create Top Up Transaction
     * @param $username
     * @param $amount
     * @param $type string
     * @return \stdClass
     */
    public static function createTopUpTransaction($username,$amount,$type = 'transfer',$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $openType = ['doku_bca_va','bni_va'];

        // rounded amount if not open
        // if (!in_array($type,$openType)) $amount = self::roundUpToNearestMultiple($amount);
        $totalPrice = $amount;
        $uniqueAmount = 0;
        if ($type=='transfer'){
            // add unique
            $isExist = true;
            $uniqueAmount = null;
            while ($isExist){
                $random = rand(25,975);
                $uniqueAmount = $amount + $random;
                $uniqueAmount = self::roundUpToNearestMultiple($uniqueAmount,5);
                $check = self::where('total_price',$uniqueAmount)->where('type','topup')->whereIn('status',['waiting'])->first();
                if (!$check) $isExist = false;
            }
            $totalPrice = $uniqueAmount;
            $uniqueAmount = $uniqueAmount - $amount;
        }

        // check method
        $paymentMethodDb = PaymentMethod::where('code',$type)->first();
        if (!$paymentMethodDb){
            $response->errorMsg = 'Invalid Payment Method';
            return $response;
        }

        $initialTopUp = $amount;
        $tmp = number_format($totalPrice);

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TOP-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'topup';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Top Up $paymentMethodDb->name $remarks $tmp at ".date('Y-m-d H:i:s');

        // create total price
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'UNPAID';
        $transactionDb->payment_type = $type === 'otto_qr' ? 'settlement' : 'non-settlement';
        $transactionDb->save();

        // get transaction Id for transaction item db
        // insert initial amount topup
        $tmp = number_format($initialTopUp);
        $transactionId = $transactionDb->id;
        $items = [];
        $item = new \stdClass();
        $item->type = 'topup';
        $item->price = $initialTopUp;
        $item->name = "Top Up $tmp";
        $item->picture = null;
        $item->params = null;
        $items[] = $item;
        if ($uniqueAmount!=0){
            // unique amount top up
            $item = new \stdClass();
            $item->type = 'topup';
            $item->price = $uniqueAmount;
            $item->name = "Unique Code $uniqueAmount";
            $item->picture = null;
            $item->params = null;
            $items[] = $item;
        }

        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create TopUp Transaction $totalPrice $remarks";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'UNPAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        $response->transactionAmount = $totalPrice;
        return $response;
    }

    /**
     * @param $username
     * @param $amount
     * @param string $type
     * @param null $remarks
     * @return \stdClass
     */
    public static function createTopUpOpenTransaction($username,$amount,$type = 'transfer',$remarks=null,$adminFee=0){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $openType = ['doku_bca_va','bni_va'];

        $totalPrice = $amount;

        // check method
        $paymentMethodDb = PaymentMethod::where('code',$type)->first();
        if (!$paymentMethodDb){
            $response->errorMsg = 'Invalid Payment Method';
            return $response;
        }
        
        $adminFee = $adminFee == null ? 0 : $adminFee;
        
        $initialTopUp = $amount + $adminFee;
        $tmp = number_format($totalPrice);

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TOP-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'topup';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Top Up $paymentMethodDb->name $remarks $tmp at ".date('Y-m-d H:i:s');

        // create total price
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'UNPAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        // insert initial amount topup
        $tmp = number_format($initialTopUp);
        $transactionId = $transactionDb->id;
        $items = [];
        $item = new \stdClass();
        $item->type = 'topup';
        $item->price = $initialTopUp;
        $item->name = "Top Up $tmp";
        $item->picture = null;
        $item->params = null;
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee_topup';
        $item->price = $adminFee;
        $item->name = "Admin Fee TopUp $tmp";
        $item->picture = null;
        $item->params = null;
        $items[] = $item;

        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create TopUp Transaction $totalPrice $remarks";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'UNPAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        $response->transactionAmount = $totalPrice;
        return $response;
    }

    /**
     * Create Referral Transaction
     * @param $username
     * @param $amount
     * @param null $remarks
     * @return \stdClass
     */
    public static function createReferralTransaction($username,$amount,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }

        $openType = ['doku_bca_va','bni_va'];

        // rounded amount if not open
        $totalPrice = $amount;
        $initialTopUp = $amount;
        $tmp = number_format($totalPrice);

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TOP-'.$userDb->id.date('ymdh').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'referral';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Top Up $remarks $tmp at ".date('Y-m-d H:i:s');

        // create total price
        $transactionDb->total_price = $totalPrice;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        // insert initial amount topup
        $tmp = number_format($initialTopUp);
        $transactionId = $transactionDb->id;
        $items = [];
        $item = new \stdClass();
        $item->type = 'referral';
        $item->price = $initialTopUp;
        $item->name = "Top Up Referral $tmp";
        $item->picture = null;
        $item->params = null;
        $items[] = $item;

        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // insert to history
        $user = $userDb->name;
        $remarks = "Create TopUp Transaction $totalPrice $remarks";
        $historyDb = TransactionHistory::createNewHistory($transactionId,$user,'PAID',$remarks);
        if (!$historyDb->isSuccess){
            $response->errorMsg = "Failed Create Transaction History";
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        $response->transactionAmount = $totalPrice;
        return $response;
    }

    /**
     * Create Additional Top Up Transaction like transfer fee etc
     * @param $username
     * @param $amount
     * @param null $description
     * @return \stdClass
     */
    public static function createAdditionalTopUpTransaction($username,$amount,$description=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get userDb
        $userDb = User::where('username',$username)->first();
        if (!$userDb){
            $response->errorMsg = 'User Not Valid';
            return $response;
        }
        $tmp = number_format($amount);

        // save to DB
        $transactionDb = new self();
        $transactionDb->users_id = $userDb->id;
        // create transaction reference
        $transactionReference = 'TOP-'.$userDb->id.date('ymdhi').Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'topup';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Top Up Fee $tmp at ".date('Y-m-d H:i:s');

        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->status = 'UNPAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        // insert initial amount topup
        $tmp = number_format($amount);
        $transactionId = $transactionDb->id;
        $items = [];
        $item = new \stdClass();
        $item->type = 'topup';
        $item->price = $amount;
        $item->name = "Top Up Fee";
        $item->picture = null;
        $item->params = null;
        $items[] = $item;
        $itemDb = TransactionItem::insertTransactionItem($transactionId,$items,$userDb->id);
        if (!$itemDb->isSuccess){
            $response->errorMsg = $itemDb->errorMsg;
            return $response;
        }

        // response
        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $transactionReference;
        return $response;
    }

    /**
     * Create Refund Transaction
     * @param $userId
     * @param $remarks
     * @param $reference
     * @param $refundAmount
     * @return \stdClass
     */
    public static function createRefundTransaction($userId, $remarks, $reference, $refundAmount)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        $transactionDb = new Transaction();
        $transactionDb->users_id = $userId;
        // create transaction reference
        $transactionReference = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);
        $transactionDb->reference = $transactionReference;
        $transactionDb->type = 'refund';
        $transactionDb->cart_id = null;
        $transactionDb->description = "Refund  $remarks from System ";
        // create total price
        $transactionDb->total_price = $refundAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;
        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->name = "Refund Transaction From $reference";
        $itemsDb->type = 'refund';
        $params = [];
        $itemsDb->params = json_encode($params);
        $itemsDb->price = $refundAmount;
        $itemsDb->save();

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        return $response;
    }

    /**
     * Refund Commission Transaction
     * @param $transactionId
     * @return \stdClass
     */
    public static function refundCommissionTransaction($transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get commission transaction with reference
        $transactionDb = Transaction::where('type','commission')
            ->where('transaction_id_reference',$transactionId)
            ->first();
        if (!$transactionDb){
            $response->errorMsg = 'Commission Transaction Not Found';
            return $response;
        }

        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->status = 'REFUND';
        $transactionDb->save();

        $response->isSuccess = true;
        return $response;
    }

    public static function releasePendingTransaction(){

    }

    /*==================================== Private Function ====================================*/

    /**
     * check Indomarco Product1
     * @param $cartId
     * @return \stdClass
     */
    private static function checkIndomarco($cartId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get cart
        $cartDb = Cart::find($cartId);
        $cartItemDb = $cartDb->cartItems;
        $items = [];
        $totalPrice = 0;

        foreach ($cartItemDb as $item){
            if ($item->type == 'popshop'){
                $params = json_decode($item->params);
                if (isset($params->sku)){
                    $sku = $params->sku;
                    $amount = $params->amount;
                    if (strpos($sku,'IDM') !== false){
                        $totalPrice += $item->price;
                        if (!isset($items[$sku])) $items[$sku] = $amount;
                        else {
                            $oldAmount = $items[$sku];
                            $newAmount = $oldAmount + $amount;
                            $items[$sku] = $newAmount;
                        }
                    }
                }
            }
        }

        // validate current cart
        foreach ($items as $key => $item){
            if ($item > 10) {
                $response->errorMsg = "Product $key Cannot More than 10 items";
                return $response;
            }
        }
        if (!empty($items)){
            if ($totalPrice<350000){
                $response->errorMsg = 'Total Product Price Less Than 350.000';
                return $response;
            }
        }


        // validate with today transaction
        // get same current user and today transaction
        $userId = $cartDb->users_id;
        $startDateTime = date('Y-m-d')." 00:00:00";
        $endDateTime = date('Y-m-d')." 23:59:59";
        $transactionDb = self::where('users_id',$userId)
            ->where('status','<>','EXPIRED')
            ->whereBetween('updated_at',[$startDateTime,$endDateTime])
            ->get();

        foreach ($transactionDb as $transaction) {
            $transactionItemDb = $transaction->items;
            foreach ($transactionItemDb as $item){
                if ($item->type == 'popshop'){
                    $params = json_decode($item->params);
                    if (isset($params->sku)){
                        $sku = $params->sku;
                        $amount = $params->amount;
                        if (strpos($sku,'IDM') !== false){
                            $totalPrice += $item->price;
                            if (!isset($items[$sku])) $items[$sku] = $amount;
                            else {
                                $oldAmount = $items[$sku];
                                $newAmount = $oldAmount + $amount;
                                $items[$sku] = $newAmount;
                            }
                        }
                    }
                }
            }
        }

        // validate
        foreach ($items as $key => $item){
            if (strpos($key,'IDM') !== false){
                if ($item > 10) {
                    $response->errorMsg = "Product $key Cannot More than 10 items";
                    return $response;
                }
            }
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Check PopShop Transaction
     * @param $cartId
     * @return \stdClass
     */
    private static function checkPopShop($cartId, $userType){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get cart
        $cartDb = Cart::find($cartId);
        $cartItemDb = $cartDb->cartItems;
        $items = [];
        $totalPrice = 0;

        foreach ($cartItemDb as $item){
            if ($item->type == 'popshop'){
                $params = json_decode($item->params);
                if (isset($params->sku)){
                    $sku = $params->sku;
                    $amount = $params->amount;
                    $totalPrice += $item->price;
                    if (!isset($items[$sku])) $items[$sku] = $amount;
                    else {
                        $oldAmount = $items[$sku];
                        $newAmount = $oldAmount + $amount;
                        $items[$sku] = $newAmount;
                    }
                }
            }
        }

        // validate current cart
        /*foreach ($items as $key => $item){
            if (strpos($key,'IDM') !== false){
                if ($item > 30) {
                    $response->errorMsg = "Product $key Cannot More than 30 items";
                    return $response;
                }
            }
        }*/
        /* if (!empty($items)){
             if ($totalPrice<350000){
                 $response->errorMsg = 'Total Product Price Less Than 350.000';
                 return $response;
             }
         }*/

        // validate with today transaction
        // get same current user and today transaction
        $userId = $cartDb->users_id;
        $startDateTime = date('Y-m-d')." 00:00:00";
        $endDateTime = date('Y-m-d')." 23:59:59";
        $transactionDb = self::where('users_id',$userId)
            ->where('status','<>','EXPIRED')
            ->whereBetween('created_at',[$startDateTime,$endDateTime])
            ->get();

        foreach ($transactionDb as $transaction) {
            $transactionItemDb = $transaction->items;
            foreach ($transactionItemDb as $item){
                if ($item->type == 'popshop'){
                    $params = json_decode($item->params);
                    if (isset($params->sku)){
                        $sku = $params->sku;
                        $amount = $params->amount;
                        $totalPrice += $item->price;
                        if (!isset($items[$sku])) $items[$sku] = $amount;
                        else {
                            $oldAmount = $items[$sku];
                            $newAmount = $oldAmount + $amount;
                            $items[$sku] = $newAmount;
                        }
                    }
                }
            }
        }

        // validate
        /*foreach ($items as $key => $item){
            if (strpos($key,'IDM') !== false){
                if ($item > 30) {
                    $response->errorMsg = "Product $key Cannot More than 30 items";
                    return $response;
                }
            }
        }*/
        
        if($userType == 'agent') {
            
            $region = \DB::select('select distinct ROUND(popbox_virtual.regions.min_purchase, 0) as "min_purchase"
                    from popbox_virtual.regions
                    join popbox_virtual.cities on popbox_virtual.cities.regions_id = popbox_virtual.regions.id
                    join popbox_virtual.lockers on popbox_virtual.lockers.cities_id = popbox_virtual.cities.id
                    join users on users.locker_id = popbox_virtual.lockers.locker_id
                    where users.id = '.$userId);
            
            \Log::debug($totalPrice. ' - ' . $region[0]->min_purchase);
            
            if(empty($region) || empty($region[0]->min_purchase)) {
                $response->errorMsg = 'Produk tidak dapat di beli pada area/kota Anda';
                return $response;
            } elseif (!empty($items) && ($totalPrice < $region[0]->min_purchase)){
                $response->errorMsg = 'Total Product Price Less Than '.number_format($region[0]->min_purchase);
                return $response;
            }
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Check Pulsa Same Transaction
     * @param array $itemParam
     * @return \stdClass
     */
    private static function checkPulsa($itemParam = []){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        if (empty($itemParam['phone'])){
            $response->errorMsg = 'Customer Phone Not Found';
            return $response;
        }
        if (empty($itemParam['product_id'])){
            $response->errorMsg = 'Product Id Not Found';
            return $response;
        }

        $customerPhone = $itemParam['phone'];
        $productId = $itemParam['product_id'];

        // check on transaction item where success / status PAID
        $previousTime = date('Y-m-d H:i:s',strtotime('-10 minutes'));

        $isFound = false;
        $lastTransactionDate = null;

        // get pulsa transaction
        $checkItems = TransactionItem::join('transactions','transactions.id','=','transaction_items.transaction_id')
            ->where('params','LIKE',"%$customerPhone%")
            ->where('transaction_items.type','pulsa')
            ->where('transaction_items.created_at','>=',$previousTime)
            ->where('transactions.status','PAID')
            ->orderBy('transaction_items.id','desc')
            ->get();

        // checking process
        foreach ($checkItems as $checkItem) {
            $tmpParams = $checkItem->params;
            if (empty($tmpParams)) continue;
            $params = json_decode($tmpParams);
            if ($params->phone == $customerPhone && $params->product_id == $productId){
                $isFound = true;
                $lastTransactionDate = $checkItem->created_at;
                break;
            }
        }

        if ($isFound){
            $nowTime = time();
            $lastTransactionTime = strtotime($lastTransactionDate);
            $secondDiff = $nowTime - $lastTransactionTime;
            $minutes = floor($secondDiff / 60);

            $response->errorMsg = 'Transaksi Gagal, Transaksi tidak bisa dilakukan dalam rentang waktu berdekatan pada nomor yang sama.';
            return $response;
        }
        $response->isSuccess = true;
        return $response;
    }

    public static function getEmailTransaction($reference) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $transactionDb = \DB::connection('mysql')
            ->select("
                select a.reference, c.name as name_user, d.locker_id, d.locker_name, d.address, c.phone, f.code as paymentcode, f.name as paymentmethod,
                    a.total_price, ifnull(cast(json_extract(b.params, '$.amount') as unsigned),0) as qtyorder,
                    b.params->>'$.sku' as sku, b.params->>'$.barcode' as barcode, b.name as productname, 
                    trim(ifnull(b.price,0)/ifnull(cast(json_extract(b.params, '$.amount') as unsigned),0))+0 as totitem, 
                    b.price as totprice, CASE WHEN d.type = 'warung' then 'PopWarung' ELSE 'Agent' END as userType
                from popbox_agent.transactions a
                left join popbox_agent.transaction_items b on a.id = b.transaction_id
                left join popbox_agent.users c on a.users_id = c.id
                left join popbox_virtual.lockers d on c.locker_id = d.locker_id
                left join payments e on a.id = e.transactions_id
                left join payment_methods f on e.payment_methods_id = f.id
                where a.reference = ?                    
            ",[$reference]);

        $response->isSuccess = true;
        $response->data = $transactionDb;

        return $response;
    }

    /**
     * Round number
     * @param $n
     * @param int $increment
     * @return int
     */
    private static function roundUpToNearestMultiple($n, $increment = 1000)
    {
        return (int) ($increment * ceil($n / $increment));
    }

    /*Relationship*/
    public function items(){
        return $this->hasMany(TransactionItem::class,'transaction_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'users_id','id');
    }

    public function payment(){
        return $this->hasOne(Payment::class,'transactions_id','id');
    }

    public function histories(){
        return $this->hasMany(TransactionHistory::class,'transactions_id','id');
    }
}
