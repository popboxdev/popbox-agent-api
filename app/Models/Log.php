<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    // define table
    protected $table = 'logs';

    /*============ Public Function ============*/
    /**
     * insert log with request
     * @param $userId
     * @param $moduleId
     * @param $request
     * @return mixed
     */
    public static function insertRequest($userId=null,$url,$request){
        $logDb = new self();
        $logDb->url = $url;
        $logDb->users_id = $userId;
        $logDb->ip = $request->ip();
        $logDb->request = json_encode($request->except('token'));
        $logDb->save();

        return $logDb->id;
    }

    /**
     * update previous request with response
     * @param $logId
     * @param $response
     */
    public static function updateResponse($logId,$response){
        $logDb = self::find($logId);
        $logDb->response = $response->content();
        $logDb->save();
        return;
    }

    /**
     * @param string $location
     * @param string $module
     * @param string $message
     */
    public static function logFile($location="",$module="",$message=""){
        if (empty($location)){
            $location = storage_path()."/logs/";
        }
        if (!is_dir($location)){
            mkdir($location,0777,true);
        }
        $user = get_current_user();
        $msg = " $message\n";
        $f = fopen($location."$module.".date('Y.m.d-').$user.'.log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
        return;
    }
    /*========== End Public Function ==========*/
}
