<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarungBanners extends Model
{
    protected $table = 'banners';
    use SoftDeletes;
}
