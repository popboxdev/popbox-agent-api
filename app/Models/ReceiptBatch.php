<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use App\Models\ReceiptItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ReceiptBatch extends Model
{
    use SoftDeletes;
    // set table
    protected $table = 'receipt_batch';
    // set date attributes
    protected $dates = ['created_at','updated_at','deleted_at'];    

    /**
     * get List ReceiptBatch
     * @param receiptbatch
     * @return \stdClass
     */
    public static function getReceiptBatch($receiptid) {
        // Get Data Receipt Batch 
        $receiptBatchDb = self::where('receipt_id', $receiptid)
            ->select( 
                DB::raw("
                    id as receipt_batch_id, batch_no, date_format(created_at, '%d/%m/%Y') as batch_date, date_format(updated_at, '%d/%m/%Y') as batch_confirm_date, status_batch as status_batch_id,
                    case 
                        when status_batch = 1 then 'Belum Dikonfirmasi'
                        when status_batch = 2 then 'Terima Sebagian'
                        when status_batch = 3 then 'Terima Semua'
                        when status_batch = 4 then 'Tolak Semua'
                        else null
                        end as status_batch,
                    users_id                            
                ")
            )
            ->get();
        
        $data = []; $no=0; $totQtyOrder=0;
        foreach($receiptBatchDb as $r) {
            $temp = [];
            $temp['batch_number'] = $r->batch_no;
            $temp['batch_date'] = $r->batch_date;
            $temp['batch_confirm_date'] = $r->batch_confirm_date;
            $temp['status_batch_id'] = $r->status_batch_id;
            $temp['status_batch'] = $r->status_batch;
            $temp['receipt_items'] = []; 

            $receiptItemDb = DB::connection('mysql')
                ->select("
                    select a.id as receipt_items_id, c.batch_no,
                        b.name as product_name, b.params,
                        ifnull(cast(json_extract(b.params, '$.amount') as unsigned),0) as qty_order,
                        ifnull(a.qty_delivery,0) as qty_delivery, ifnull(a.qty_receipt,0) as qty_receipt, ifnull(a.gap_order,0) as gap_order, a.transaction_items_id, a.receipt_batch_id, c.batch_no
                    from receipt_items a
                    left join transaction_items b on a.transaction_items_id = b.id
                    left join receipt_batch c on a.receipt_batch_id = c.id
                    where a.receipt_batch_id = ?
                ", [$r->receipt_batch_id]);
                            
            foreach($receiptItemDb as $item) {
                $temp2 = [];
                $params = json_decode($item->params);

                $totOrder = 0;
                $gapOrder = 0;
                $batchNo = ((int) preg_replace('/[^0-9]/', '', $item->batch_no) - 1);                
                if($no == 0) {
                    $batchNo = 'BATCH-1';
                    $totOrder = $item->qty_order;
                    $gapOrder = $item->gap_order;
                }
                else {
                    $batchNo = 'BATCH-'.$batchNo; 
                    $totOrder = self::getReceiptBatchByBatchNo($batchNo, $item->transaction_items_id)->gap_order;
                    $gapOrder = $totOrder - (empty($item->qty_receipt) ? 0 : $item->qty_receipt);
                }

                $temp2['item_id'] = $item->receipt_items_id;
                $temp2['product_id'] = $params->product_id;
                $temp2['sku'] = $params->sku;
                $temp2['product_name'] = $item->product_name;
                $temp2['tot_order'] = empty($totOrder) ? 0 : $totOrder;
                $temp2['qty_order'] = $item->qty_order;
                $temp2['qty_delivery'] = $item->qty_delivery;
                $temp2['qty_receipt'] = $item->qty_receipt;
                $temp2['gap_qty'] = $gapOrder;
                $temp2['product_type'] = null;
                $temp2['product_bom'] = [];

                if(isset($params->product_type)) {
                    $temp2['product_type'] = $params->product_type;
                    if($params->product_type == 'bom') {
                        foreach($params->product_item as $s) {
                            $bomItem = [];
                            $bomItem['product_id'] = $s->product_id;
                            $bomItem['qty'] = (empty($s->qty) ? 0 : $s->qty) * (empty($params->amount) ? 0 : $params->amount);
                            $bomItem['sku'] = $s->sku;
                            $bomItem['product_name'] = $s->product_name;

                            $temp2['product_bom'][] = $bomItem;
                        }    
                    }
                }            
                $temp['receipt_items'][] = $temp2;                                
            }            

            $no++;
            $data[] = $temp;            
        }

        return $data;
    }

    /**
     * get ReceiptBatch By Batch Number
     * @param receiptbatch
     * @return \stdClass
     */
    public static function getReceiptBatchByNumber($receiptid, $batchNumber) {
        // Get Data Receipt Batch 
        $receiptBatchDb = self::where('receipt_id', $receiptid)
            ->where('batch_no', $batchNumber)
            ->select( 
                DB::raw("
                    id as receipt_batch_id, batch_no, updated_at as batch_date, status_batch as status_batch_id,                    
                    case 
                        when status_batch = 1 then 'Belum Dikonfirmasi'
                        when status_batch = 2 then 'Terima Sebagian'
                        when status_batch = 3 then 'Terima Semua'
                        when status_batch = 4 then 'Tolak Semua'
                        else null
                        end as status_batch,
                    users_id                            
                ")
            )
            ->first();
        
        $temp = [];
        $temp['batch_number'] = $receiptBatchDb->batch_no;
        $temp['batch_date'] = $receiptBatchDb->batch_date;
        $temp['batch_date'] = $receiptBatchDb->batch_date;
        $temp['status_batch_id'] = $receiptBatchDb->status_batch_id;
        $temp['status_batch'] = $receiptBatchDb->status_batch;
        $temp['receipt_items'] = []; 

        // Get Data Receipt Item
        $receiptItemDb = ReceiptItem::leftJoin('transaction_items', 'receipt_items.transaction_items_id', '=', 'transaction_items.id')
            ->where('receipt_items.receipt_batch_id', $receiptBatchDb->receipt_batch_id)
            ->select(
                DB::raw("
                    receipt_items.id AS receipt_items_id,
                    transaction_items.name AS product_name, transaction_items.params,
                    ifnull(receipt_items.qty_delivery,0) as qty_delivery, ifnull(receipt_items.qty_receipt,0) as qty_receipt,
                    (IFNULL(receipt_items.qty_delivery, 0) - IFNULL(receipt_items.qty_receipt, 0)) AS gap_qty
                ")
            )
            ->get();

        foreach($receiptItemDb as $item) {
            $temp2 = [];
            $params = json_decode($item->params);
            $amount = $params->amount;

            $temp2['item_id'] = $item->receipt_items_id;
            $temp2['product_id'] = $params->product_id;
            $temp2['sku'] = $params->sku;
            $temp2['product_name'] = $item->product_name;
            $temp2['qty_delivery'] = $item->qty_delivery;
            $temp2['qty_receipt'] = $item->qty_receipt;
            $temp2['gap_qty'] = $item->gap_qty;
            $temp2['product_type'] = null;
            $temp2['product_bom'] = [];

            if(isset($params->product_type)) {
                $temp2['product_type'] = $params->product_type;
                if($params->product_type == 'bom') {
                    foreach($params->product_item as $s) {
                        $bomItem = [];
                        $bomItem['product_id'] = $s->product_id;
                        $bomItem['qty'] = (empty($item->qty_receipt) ? 0 : $item->qty_receipt) * (empty($s->qty) ? 0 : $s->qty);
                        $bomItem['sku'] = $s->sku;
                        $bomItem['product_name'] = $s->product_name;

                        $temp2['product_bom'][] = $bomItem;
                    }    
                }
            }                    
            $temp['receipt_items'][] = $temp2;
        }

        return $temp;
    }

    /**
     * get ReceiptBatch By Batch Number
     * @param receiptbatch
     * @return \stdClass
     */
    public static function getReceiptBatchByBatchNo($batchNo, $transactionItemID) {
        $receiptBatchDb = self::where('receipt_batch.batch_no', $batchNo)
            ->where('receipt_items.transaction_items_id', $transactionItemID)
            ->leftJoin('receipt_items', 'receipt_batch.id', '=', 'receipt_items.receipt_batch_id')
            ->select( 
                DB::raw("
                    receipt_items.id as receipt_items_id, receipt_items.gap_order
                ")
            )->first();
        return $receiptBatchDb;
    }

    /*Relationship*/
    public function items(){
        return $this->hasMany(ReceiptItem::class,'receipt_batch_id','id');
    }

    public function receipt(){
        return $this->belongsTo(Receipt::class,'receipt_id','id');
    }
}
