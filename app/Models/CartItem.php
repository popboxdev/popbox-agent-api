<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{
    use SoftDeletes;
    // set table
    protected $table = 'cart_items';
    protected $dates = ['deleted_at'];
    /*============ Public function ============*/

    /**
     * insert new items to cart
     * @param $cartId
     * @param $itemName
     * @param $itemType
     * @param $itemPrice
     * @param array $itemParams
     * @param null $itemPicture
     * @return \stdClass
     */
    public static function insertItem($cartId,$itemName,$itemType,$itemPrice,$itemParams=[],$itemPicture = null){
        // generate response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        try {
            
            // check Cart Id
            $cartDb = Cart::find($cartId);
            if (!$cartDb){
                $response->errorMsg = 'Invalid Cart Reference';
                return $response;
            }
            
            if ($itemType=='pulsa'){
                // get operator
                if (!empty($itemParams['operator'])){
                    $itemPicture = asset('img/logo_operator')."/".$itemParams['operator'].".png";
                }
            }
            // change price popshop to total price
            if ($itemType == 'popshop'){
                // check Existing Item Popshop
                $cartDb = self::where('type','popshop')
                ->where('carts_id', $cartId)
                ->whereRaw("json_extract(params, '$.sku') = ?", [$itemParams['sku']]);
                
                if($cartDb->count() > 0) {
                    $r = $cartDb->first();
                    $cartParam = json_decode($r->params);
                    $amountExist = isset($cartParam->amount) ? $cartParam->amount : 0;
                    $amount = (int) $itemParams['amount'] + (int) $amountExist;
                    
                    $itemParams['amount'] = $amount;
                    $itemPrice = $itemPrice * $amount;
                    
                    $result = self::where('id', $r->id)
                    ->update([
                        'params' => is_string($itemParams) ? $itemParams : json_encode($itemParams),
                        'price' => $itemPrice,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                    
                    if($result) {
                        $response->isSuccess = true;
                    }
                    else {
                        $response->isSuccess = false;
                    }
                    return $response;
                }
                $amount = $itemParams['amount'];
                $itemPrice = $itemPrice * $amount;
            }
            
            // insert into db
            $data = new self();
            $data->carts_id = $cartId;
            $data->name = $itemName;
            $data->type = $itemType;
            $data->picture = $itemPicture;
            $data->params = is_string($itemParams) ? $itemParams : json_encode($itemParams);
            $data->price = $itemPrice;
            $data->save();
            
            $response->isSuccess = true;
        } catch (\Exception $e) {
            Log::debug("Line #".$e->getLine().": ".$e->getMessage());
            $response->errorMsg = $e->getMessage();
        }
        return $response;
    }

    public static function removeItem($itemId){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get item detail
        $itemDb = self::find($itemId);
        if (empty($itemDb)){
            $response->errorMsg = 'Invalid Item ID';
            return $response;
        }

        // delete item
        $itemDb->delete();

        $response->isSuccess = true;
        return $response;
    }
    
    public static function updateItem($itemId, $amount=0, $itemPrice){
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        // get item detail
        $item = self::find($itemId);
        if (empty($item)){
            $response->errorMsg = 'Invalid Item ID';
            return $response;
        }
        
        $params = json_decode($item->params, true);
        
        $params['amount'] = $amount;
        
        $item->params = is_string($params) ? $params : json_encode($params);
        $item->price = $itemPrice;
        $item->save();
        
        $response->isSuccess = true;
        return $response;
    }

    /*========== End Public function ==========*/
    /*Relationship*/
    public function cart(){
        return $this->belongsTo(Cart::class,'id','carts_id');
    }
}
