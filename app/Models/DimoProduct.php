<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class DimoProduct extends Model
{
    // set connection
    protected $connection = 'popbox_db';
    // set table
    protected $table = 'dimo_products';
    protected $primaryKey = 'sku';
    public $incrementing = false;

    public static function getProduct($input=[]){
        $category= empty($input['category']) ? null : $input['category'];
        $name = empty($input['name']) ? null : $input['name'];
        $sku = empty($input['sku']) ? null : $input['sku'];
        $minPrice = empty($input['min_price']) ? null : $input['min_price'];
        $maxPrice = empty($input['max_price']) ? null : $input['max_price'];
        $sort = empty($input['sort']) ? 'dimo_products.id' : $input['sort'];
        $sortType = empty($input['sort_type']) ? 'asc' : $input['sort_type'];
        $take= empty($input['limit']) ? null : $input['limit'];

        $cacheName = "popshop-product-$category-$name-$sku-$minPrice-$maxPrice-$sort-$sortType-$take";

        // data product
        $productDb = Cache::remember($cacheName,10,function() use($category,$name,$sku,$minPrice,$maxPrice,$take,$sort,$sortType){
            return self::join('dimo_product_category','dimo_products.sku','=','dimo_product_category.product_sku')
                ->join('dimo_product_category_name','dimo_product_category.id_category','=','dimo_product_category_name.id_category')
                ->join('dimo_product_stock','dimo_products.id','=','dimo_product_stock.product_id')
                ->when($category,function ($query) use($category){
                    if (is_array($category)) return $query->whereIn($category);
                    return $query->where('category_name','LIKE',"%$category%");
                })->when($name,function ($query) use($name){
                    return $query->where('name','LIKE',"%$name%");
                })->when($sku,function ($query) use($sku){
                    return $query->where('sku','LIKE',$sku);
                })->when($minPrice,function($query) use($minPrice){
                    return $query->where('price','>',$minPrice);
                })->when($maxPrice,function ($query) use($maxPrice){
                    return $query->where('price','<',$maxPrice);
                })->when($take,function ($query) use($take){
                    return $query->take($take);
                })->orderBy($sort,$sortType)
                ->get();
        });
        return $productDb;
    }

    /*Relationship*/
    public function categories(){
        return $this->belongsToMany(DimoCategoryName::class,'dimo_product_category','product_sku','id_category');
    }
}
