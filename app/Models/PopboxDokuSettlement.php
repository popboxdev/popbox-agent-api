<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PopboxDokuSettlement extends Model
{
    // set connection
    protected $connection = 'popbox_db';
    protected $table = 'doku_cc_settlement';
}
