<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use App\Models\ReceiptBatch;
use App\Models\ReceiptItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ReceiptItem extends Model
{
    use SoftDeletes;
    // set table
    protected $table = 'receipt_items';
    // set date attributes
    protected $dates = ['created_at','updated_at','deleted_at'];    

    public static function updateBatchItem($params) {
        // generate response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = [];

        $tbReceiptItems = ReceiptItem::where('id', $params['id'])
            ->update([
                'qty_receipt' => $params['qty_receipt'],
                'gap_order' => $params['gap_qty']
            ]);

        if($tbReceiptItems) {
            $response->isSuccess = true;
            $tbReceiptItems = ReceiptItem::where('id', $params['id'])
                ->select(
                    DB::raw("(ifnull(qty_delivery,0)-ifnull(qty_receipt,0)) as gap")
                )->first();                
            $response->data = $tbReceiptItems;
        }
        else {
            $response->isSuccess = false;
            $response->errorMsg = 'Data has been failure';
        }

        return $response;
    }

    public function receiptBatch(){
        return $this->belongsTo(ReceiptBatch::class,'receipt_batch_id','id');
    }
}
