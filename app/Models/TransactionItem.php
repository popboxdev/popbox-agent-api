<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    //set table
    protected $table = 'transaction_items';

    /**
     * create transaction item
     * @param $transactionId
     * @param array $items
     * @param $userId
     * @return \stdClass
     */
    public static function insertTransactionItem($transactionId,$items=[],$userId){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $purchaseList = ['pulsa','popshop'];
        $paymentList = ['electricity','electricity_postpaid','pdam','bpjs_kesehatan','telkom_postpaid'];


        foreach ($items as $item) {
            // calculate schema
            $commissionId = null;
            $commissionAmount = null;
            $transactionType = 'purchase';
            if (in_array($item->type,$purchaseList)) $transactionType = 'purchase';
            elseif ($item->type == 'delivery') $transactionType = 'delivery';
            elseif (in_array($item->type,$paymentList)) $transactionType = 'payment';

            if ($item->type == 'pulsa'){
                $itemParams = json_decode($item->params);
                $params = [];
                $params['transaction_type'] = $transactionType;
                $params['transaction_item_type'] = 'pulsa';
                $params['user_id'] = $userId;
                $params['customer_phone'] = $itemParams->phone;
                if (!empty($itemParams->product_id)) $params['sepulsa_product_id'] = $itemParams->product_id;
                $basicPrice = $item->price;
                $publishPrice = $item->price;
                $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $commissionId = $commission->commissionId;
                    $commissionAmount = $commission->commission;
                }
            }
            elseif (in_array($item->type,$paymentList)){
                $itemParams = json_decode($item->params);
                $params = [];
                $params['transaction_type'] = $transactionType;
                $params['transaction_item_type'] = $item->type;
                $params['user_id'] = $userId;
                if (!empty($itemParams->product_id)) $params['sepulsa_product_id'] = $itemParams->product_id;
                $basicPrice = $item->price;
                $publishPrice = $item->price;
                $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $commissionId = $commission->commissionId;
                    $commissionAmount = $commission->commission;
                }
            }
            else {
                $itemParams = json_decode($item->params);
                $params = [];
                $params['transaction_type'] = $transactionType;
                $params['transaction_item_type'] = $item->type;
                $params['user_id'] = $userId;
                $basicPrice = $item->price;
                $publishPrice = $item->price;
                $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $commissionId = $commission->commissionId;
                    $commissionAmount = $commission->commission;
                }
            }

            $itemDb = new self();
            $itemDb->transaction_id = $transactionId;
            $itemDb->commission_schemas_id = $commissionId;
            $itemDb->commission_amount = $commissionAmount;
            $itemDb->name = $item->name;
            $itemDb->type = $item->type;
            $itemDb->picture = $item->picture;
            $itemDb->params = $item->params;
            $itemDb->price = $item->price;
            $itemDb->save();
        }
        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }
}
