<?php

namespace App\Http\Controllers;

use App\Http\Helpers\APIv2;
use App\Jobs\CreateTransactionNotification;
use App\Jobs\ProcessTransaction;
use App\Jobs\SendFCM;
use App\Models\CommissionSchema;
use App\Models\DimoCategoryName;
use App\Models\DimoProduct;
use App\Models\NotificationFCM;
use App\Models\Payment;
use App\Models\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\MessageService;
use App\Models\MessageServiceDetail;

class ServiceController extends Controller
{
    private $popShopCategory = ['Grocery','Paket Promo','Rokok'];

    /**
     * get pulsa operator list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPulsaOperator(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get operator and prefix
        $operatorDb = DB::connection('popbox_db')->table('tb_sepulsa_prefix')->get();

        $operator = array();
        $list = array();
        //echo "<pre>".print_r($StationMap->queryAll(),true);die;
        foreach ($operatorDb as $item)
        {
            if (!isset($operator[$item->operator]))
            {
                $i = count($operator);
                $operator[$item->operator] = $i;
            }
            $i = $operator[$item->operator];

            $list[$i]['operator'] = utf8_encode($item->operator);
            $list[$i]['picture'] = asset('img/logo_operator')."/".$item->operator.".png";
            $list[$i]['prefix'][] = $item->number;
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $list];
        return response()->json($resp);
    }

    /**
     * get pulsa product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPulsaProduct(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get variable
        $phone = $request->input('phone',null);
        $operator = $request->input('operator',null);
        $username = $request->input('username');

        if (empty($phone) && empty($operator)){
            $message = "Phone or Operator Required";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // push to API
        $apiv2 = new APIv2();
        $result = $apiv2->getPulsaProduct($phone,$operator);
        if (empty($result)){
            $message = 'Failed to get Pulsa Product';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            if(strtoupper($message) == strtoupper('Operator Not Found')){
                $message = "Operator tidak ditemukan di sistem";
            }
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $userId = $userDb->id;

        $data = $result->data;
        $pulsaList = $data->pulsa_list;
        foreach ($pulsaList as $item){
            $basicPrice = $item->price;
            $publishPrice = $item->nominal;

            // $commission= CommissionSchema::calculateSchema('pulsa',$basicPrice,$publishPrice);
            // $item->commission = $commission->commission;

            // calculate new param
            $params = [];
            $params['transaction_type'] = 'purchase';
            $params['transaction_item_type'] = 'pulsa';
            $params['user_id'] = $userId;
            $params['customer_phone'] = $phone;
            $params['sepulsa_product_id'] = $item->product_id;
            $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
            $item->commission = $commission->commission;
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get PopShop Category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPopShopCategory(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $popShopCategory = DimoCategoryName::where('category_name','LIKE','Grocery%')
            ->where('is_enabled', '1')
            ->orWhere('category_name','LIKE','Rokok')
            ->orderBy('no_list', 'asc')
            ->get();

        $result = [];
        foreach ($popShopCategory as $item){
            $tmp = new \stdClass();
            $tmp->category_name = $item->category_name;
            $result[] = $tmp;
        }
        if (empty($result)){
            $message = "Empty Category";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $result];
        return response()->json($resp);
    }

    /**
     * get PopShop product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPopShopProduct(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userId = $userDb->id;

        $productList = [];
        if (empty($input['category'])) $input['category'] = "Grocery";
        $page = $request->input('page',1);
        // get to DB
        $productDb = DimoProduct::getProduct($input);

        // create response
        $startTime = microtime(true);
        foreach ($productDb as $item) {
            $tmp = new \stdClass();
            $tmp->sku = $item->sku;
            $tmp->category = $item->category_name;
            $tmp->name = $item->name;
            $tmp->description = $item->description;
            $tmp->image = "https://shop.popbox.asia/assets/images/uploaded/".preg_replace('/\s+/', '%20', $item->image);
            $tmp->stock = $item->stock;
            $tmp->price = $item->price - $item->discount;
            $tmp->discount = $item->discount;
            // calculate commission
            $basicPrice = $tmp->price;
            $publishPrice = $tmp->price;
            $params = [];
            $params['transaction_type'] = 'purchase';
            $params['transaction_item_type'] = 'popshop';
            $params['user_id'] = $userId;
            //$commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
            $tmp->commission = 0;
            $productList[] = $tmp;
        }

        if (empty($productList)){
            $message = "Product Not Found";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $endTime = microtime(true);

        $currentPage = 0;
        $pages = [1];
        $totalPages =1;
        $productList = collect($productList);
        $totalData = count($productList);
        if ($page != 0){
            $pageList = $productList->chunk(10);
            $pages = [];
            foreach ($pageList as $key => $value){
                $pages[] = $key+1;
            }
            $chunk = $productList->forPage($page,10);
            $productList = $chunk->values()->all();
            $totalPages = count($pages);
            $currentPage = $page;
        }

        $resp=[
            'response' => [
                'code' => 200,
                'message' =>null
            ],
            'total_data'=>$totalData,
            'pages'=>$pages,
            'current_page'=>$currentPage,
            'total_pages'=>$totalPages,
            'data' => $productList
        ];
        return response()->json($resp);
    }

    /**
     * Get PLN Pre Paid Product
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPLNPrePaidProduct(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userId = $userDb->id;

        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPrePaidProduct();
        if (empty($result)){
            $message = 'Failed to get PLN PrePaid Product';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data;
        $productList = $data->product_list;
        foreach ($productList as $item){
            $basicPrice = $item->price;
            $publishPrice = $item->nominal;
            $params = [];
            $params['transaction_type'] = 'payment';
            $params['transaction_item_type'] = 'electricity';
            $params['user_id'] = $userId;
            $params['sepulsa_product_id'] = $item->product_id;
            $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
            $item->commission = $commission->commission;
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * get PLN PrePaid Inquiry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPLNPrePaidInquiry(Request $request){
        //required param list
        $required = ['meter_number','product_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $meterNumber = $request->input('meter_number');
        $productId = $request->input('product_id');
        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userId = $userDb->id;

        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPrePaidInquiry($meterNumber,$productId);
        if (empty($result)){
            $message = 'Failed to get PLN Pre Paid Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get PLN PostPaid Inquiry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPLNPostPaidInquiry(Request $request){
        //required param list
        $required = ['meter_number'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $meterNumber = $request->input('meter_number');

        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPostPaidInquiry($meterNumber);
        if (empty($result)){
            $message = 'Failed to get PLN Pre Paid Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get BPJS Inquiry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBPJSInquiry(Request $request){
        //required param list
        $required = ['bpjs_number'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $bpjsNumber = $request->input('bpjs_number');

        $apiv2 = new APIv2();
        $result = $apiv2->getBPJSInquiry($bpjsNumber);
        if (empty($result)){
            $message = 'Failed to get BPJS Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get Telkom Inquiry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTelkomInquiry(Request $request){
        //required param list
        $required = ['telkom_number'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $telkomNumber = $request->input('telkom_number');
        $apiv2 = new APIv2();
        $result = $apiv2->getTelkomInquiry($telkomNumber);
        if (empty($result)){
            $message = 'Failed to get PDAM Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get PDAM Operator
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPDAMOperator(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $apiv2 = new APIv2();
        $result = $apiv2->getPDAMOperator();
        if (empty($result)){
            $message = 'Failed to get PDAM Product';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data;

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Get PDAM Inquiry
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPDAMInquiry(Request $request){
        //required param list
        $required = ['pdam_number','operator_code'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $pdamNumber = $request->input('pdam_number');
        $operatorCode = $request->input('operator_code');
        $apiv2 = new APIv2();
        $result = $apiv2->getPDAMInquiry($pdamNumber,$operatorCode);
        if (empty($result)){
            $message = 'Failed to get PDAM Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Post PLN Pre Paid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postPLNPrePaid(Request $request){
        //required param list
        $required = ['meter_number','product_id','phone','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $meterNumber = $request->input('meter_number');
        $productId = $request->input('product_id');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        // get product
        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPrePaidProduct();
        if (empty($result)){
            $message = 'Failed to get PLN PrePaid Product';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data;
        $productList = $data->product_list;
        $selectedProduct = null;
        foreach ($productList as $item){
            if ($item->product_id == $productId) $selectedProduct = $item;
        }

        if (empty($selectedProduct)){
            $message = "Product Not Found";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPrePaidInquiry($meterNumber,$productId);
        if (empty($result)){
            $message = 'Failed to get PLN Pre Paid Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        // create transaction
        $itemName = $selectedProduct->label;
        $itemType = 'electricity';
        $itemTotalPrice = $data->total_amount;
        $userDb = User::where('username',$username)->first();
        $agentName = $userDb->locker->locker_name;
        $customerEmail = $request->input('customer_email',null);

        // create items transaction
        $items = [];
        $item = new \stdClass();
        $item->type = 'electricity';
        $item->price = $data->base_amount;
        $item->name = $itemName;
        $item->picture = null;
        $itemParam = [];
        $itemParam['meter_number'] = $meterNumber;
        $itemParam['product_id'] = $productId;
        $itemParam['phone'] = $phone;
        $itemParam['agent_name'] = $agentName;
        $itemParam['customer_email'] = $customerEmail;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee';
        $item->price = $data->admin_charge;
        $item->name = "Admin Fee $itemName";
        $item->picture = null;
        $itemParam = [];
        $itemParam['meter_number'] = $meterNumber;
        $itemParam['product_id'] = $productId;
        $itemParam['phone'] = $phone;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Post PLN Post Paid
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postPLNPostPaid(Request $request){
        //required param list
        $required = ['meter_number','phone','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $meterNumber = $request->input('meter_number');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        $apiv2 = new APIv2();
        $result = $apiv2->getPLNPostPaidInquiry($meterNumber);
        if (empty($result)){
            $message = 'Failed to get PLN Pre Paid Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        // create transaction
        $itemName = "PLN Post Paid $meterNumber";
        $itemType = 'electricity_postpaid';
        $itemTotalPrice = $data->total_amount;
        $userDb = User::where('username',$username)->first();
        $agentName = $userDb->locker->locker_name;
        $customerEmail = $request->input('customer_email',null);

        // create items transaction
        $items = [];
        $item = new \stdClass();
        $item->type = 'electricity_postpaid';
        $item->price = $data->total_amount - $data->admin_charge;
        $item->name = $itemName;
        $item->picture = null;
        $itemParam = [];
        $itemParam['meter_number'] = $meterNumber;
        $itemParam['phone'] = $phone;
        $itemParam['agent_name'] = $agentName;
        $itemParam['customer_email'] = $customerEmail;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee';
        $item->price = $data->admin_charge;
        $item->name = "Admin Fee $itemName";
        $item->picture = null;
        $itemParam = [];
        $itemParam['meter_number'] = $meterNumber;
        $itemParam['phone'] = $phone;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * BPJS Kesehatan
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postBPJS(Request $request){
        //required param list
        $required = ['bpjs_number','phone','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $bpjsNumber = $request->input('bpjs_number');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        $apiv2 = new APIv2();
        $result = $apiv2->getBPJSInquiry($bpjsNumber);
        if (empty($result)){
            $message = 'Failed to get PLN Pre Paid Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        // create transaction
        $itemName = "BPJS Kesehatan $bpjsNumber";
        $itemType = 'bpjs_kesehatan';
        $itemTotalPrice = $data->total_amount;
        $userDb = User::where('username',$username)->first();
        $agentName = $userDb->locker->locker_name;
        $customerEmail = $request->input('customer_email',null);

        // create items transaction
        $items = [];
        $item = new \stdClass();
        $item->type = 'bpjs_kesehatan';
        $item->price = $data->base_amount;
        $item->name = $itemName;
        $item->picture = null;
        $itemParam = [];
        $itemParam['bpjs_number'] = $bpjsNumber;
        $itemParam['phone'] = $phone;
        $itemParam['agent_name'] = $agentName;
        $itemParam['customer_email'] = $customerEmail;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee';
        $item->price = $data->admin_charge;
        $item->name = "Admin Fee $itemName";
        $item->picture = null;
        $itemParam = [];
        $itemParam['bpjs_number'] = $bpjsNumber;
        $itemParam['phone'] = $phone;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * post PDAM Transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postPDAM(Request $request){
        //required param list
        $required = ['pdam_number','operator_code','phone','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $pdamNumber = $request->input('pdam_number');
        $operatorCode = $request->input('operator_code');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        $apiv2 = new APIv2();
        $result = $apiv2->getPDAMInquiry($pdamNumber,$operatorCode);
        if (empty($result)){
            $message = 'Failed to get PDAM Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        // create transaction
        $itemName = "PDAM $operatorCode $pdamNumber";
        $itemType = 'pdam';
        $itemTotalPrice = $data->total_amount;
        $userDb = User::where('username',$username)->first();
        $agentName = $userDb->locker->locker_name;
        $customerEmail = $request->input('customer_email',null);

        // create items transaction
        $items = [];
        $item = new \stdClass();
        $item->type = 'pdam';
        $item->price = $data->total_amount - $data->admin_charge; // ini salah
        $item->name = $itemName;
        $item->picture = null;
        $itemParam = [];
        $itemParam['pdam_number'] = $pdamNumber;
        $itemParam['operator_code'] = $operatorCode;
        $itemParam['phone'] = $phone;
        $itemParam['agent_name'] = $agentName;
        $itemParam['customer_email'] = $customerEmail;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee';
        $item->price = $data->admin_charge;
        $item->name = "Admin Fee $itemName";
        $item->picture = null;
        $itemParam = [];
        $itemParam['pdam_number'] = $pdamNumber;
        $itemParam['operator_code'] = $operatorCode;
        $itemParam['phone'] = $phone;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * post telkom
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postTelkom(Request $request){
        //required param list
        $required = ['telkom_number','phone','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $telkomNumber = $request->input('telkom_number');
        $phone = $request->input('phone');
        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        $apiv2 = new APIv2();
        $result = $apiv2->getTelkomInquiry($telkomNumber);
        if (empty($result)){
            $message = 'Failed to get Telkom Inquiry';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        // create transaction
        $itemName = "Telkom $telkomNumber";
        $itemType = 'pdam';
        $itemTotalPrice = $data->total_amount;
        $userDb = User::where('username',$username)->first();
        $agentName = $userDb->locker->locker_name;
        $customerEmail = $request->input('customer_email',null);

        // create items transaction
        $items = [];
        $item = new \stdClass();
        $item->type = 'telkom_postpaid';
        $item->price = $data->base_amount;
        $item->name = $itemName;
        $item->picture = null;
        $itemParam = [];
        $itemParam['telkom_number'] = $telkomNumber;
        $itemParam['phone'] = $phone;
        $itemParam['agent_name'] = $agentName;
        $itemParam['customer_email'] = $customerEmail;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        $item = new \stdClass();
        $item->type = 'admin_fee';
        $item->price = $data->admin_charge;
        $item->name = "Admin Fee $itemName";
        $item->picture = null;
        $itemParam = [];
        $itemParam['telkom_number'] = $telkomNumber;
        $itemParam['phone'] = $phone;
        $item->params = json_encode($itemParam);
        $items[] = $item;

        DB::beginTransaction();
        // create transaction
        $transactionDb = Transaction::createSingleTransactionMultipleItems($username,$itemName,$itemType,$itemTotalPrice,$items);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        // dispatch job for push to services
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        dispatch(new ProcessTransaction($transactionDb));
        dispatch(new CreateTransactionNotification($transactionDb));

        // create parameter for FCM Notification
        $userDb = User::where('username',$username)->first();
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                NotificationFCM::createTransactionNotification($transactionDb->id,$fcmToken,$data,'Transaksi Sedang di Proses','process','Transactions');
            }
            dispatch(new SendFCM());
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }
    
    public function postStoreFile(Request $request){
        $image = $request->get('image');
        $filename = $request->get('filename');
        $dirPath = $request->get('dir_path');
        
        Storage::disk($dirPath)->put($filename, base64_decode($image));
        
        $resp = ['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }
    
    public function postStoreFiles(Request $request){
        $files = $request->get('file');
        $filename = $request->get('filename');
        $dirPath = $request->get('dir_path');
        
        if(!is_array($files)) $files  = [$files];
        
        foreach ($files as $file){
            Storage::disk($dirPath)->put($filename, base64_decode($file));
        }
        
        $resp = ['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }
    
    public function postSendMessage(Request $request){
        $response = MessageService::createMessageNotification($request->get('message_id'), $request->get('module'));
        
        dispatch(new SendFCM());
        $resp = ['response' => ['code' => 200,'message' =>null], 'data' => ['count' => $response->count]];
        return response()->json($resp);
    }
    
    public function getMessage(Request $request) {
        $locker_id = $request->get('username');
        
        $message = collect(DB::connection('mysql')->select("
                select ms.id, ms.title, ms.body_message, ms.url_loc, ms.menu_app, ms.url, ms.image_name, msd.message_opened, ms.btn_name, DATE_FORMAT(ms.created_at, '%d-%M-%Y %H:%i:%s') as 'created_at'
                from message_service ms
                join message_service_detail msd on ms.id = msd.message_service_id
                join users u on u.locker_id = msd.recipient
                where u.phone = ? order by ms.created_at desc", [$locker_id])
            )->map(function($item) {
                return [
                    'message_id'        => $item->id,
                    'message_title'     => $item->title,
                    'body_message'      => $item->body_message,
                    'url_button'        => $item->url_loc,
                    'menu_button'       => $item->menu_app,
                    'direct_link'       => $item->url,
                    'image_link'        => env('AGENT_URL').'upload/'.$item->image_name,
                    'message_opened'    => $item->message_opened == 1 ? true : false,
                    'btn_name'          => $item->btn_name,
                    'message_created'   => $item->created_at,
                ];
            });
        
        $resp = [
            'response' => ['code' => 200,'message' =>null], 
            'total_data' => count($message), 
            'data' => $message
        
        ];
        
        return response()->json($resp);
    }
    
    public function updateMessage(Request $request) {
        $locker_id = $request->get('locker_id');
        $message_id = $request->get('message_id');
        $is_message_opened = $request->get('is_message_opened');
        $is_link_clicked = $request->get('is_link_clicked');
        $result = '';
        
        if(!is_array ($message_id)) $message_id = [$message_id];
        
        foreach ($message_id as $id){
            if($is_message_opened || $is_link_clicked){
                $user = User::where('locker_id', $locker_id)->first();
                $message_detail = MessageServiceDetail::where('message_service_id', $id)->where('recipient', $locker_id)->first();
                if(!empty($message_detail)){
                    if($is_message_opened) $message_detail->message_opened = 1;
                    if($is_link_clicked) $message_detail->url_clicked = 1;
                    $message_detail->updated_by = $user->name;
                    $message_detail->save();
                    
                    $result = '';
                }
            }
        }
        
        $resp = [
            'response' => ['code' => 200,'message' => $result],
            'data' => [],
        ];
        
        return response()->json($resp);
    }
}
