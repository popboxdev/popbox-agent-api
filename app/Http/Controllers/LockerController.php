<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\VirtualLocker;
use Illuminate\Support\Facades\DB;
use App\Models\Locker;
use App\Http\Helpers\Api;
use Illuminate\Support\Facades\Log;

class LockerController extends Controller
{
    public function getLockerLocation(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get locker list
        $lockerDb = DB::connection('popbox_db')->table('locker_locations')->get();
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $lockerDb];
        return response()->json($resp);
    }

    public function getLocker(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $userInput = $request->input('user_input');

        $field = filter_var($userInput, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $checkAgentUser = User::where($field,$userInput)->first();
        if (!$checkAgentUser){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $agentId = $checkAgentUser->id;
        $agentDb = User::find($agentId);

        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $response->locker_id = $agentDb->locker_id;
        $response->status = $agentDb->status;
        $response->id_number = $agentDb->id_number;
        $response->balance = 0;

        // get virtual locker data
        if (!empty($agentDb->locker_id)){
            $response->balance = $agentDb->locker->balance;
            $lockerDb = VirtualLocker::where('locker_id',$agentDb->locker_id)->first();
            $response->locker_name = null;
            $response->locker_address = null;
            if ($lockerDb){
                $response->locker_name = $lockerDb->locker_name;
                $response->locker_address = $lockerDb->address;
                $response->locker_address_detail = $lockerDb->detail_address;
                $services = [];
                foreach ($lockerDb->services as $item){
                    $services[] = $item->name;
                }
                $response->services = $services;
                $days = [];
                $openHour = [];
                $closeHour = [];
                foreach ($lockerDb->operational_time as $key => $time){
                    $days[$key] = $time->day;
                    $openHour[$key] = empty($time->open_hour) ? null : sprintf("%02d",$time->open_hour);
                    $closeHour[$key] = empty($time->close_hour) ? null : sprintf("%02d",$time->close_hour);
                }
                $response->days = $days;
                $response->open_hour = $openHour;
                $response->close_hour = $closeHour;
            }
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$response]];
        return response()->json($resp);
    }
    
    public function open(Request $request) {
        
        $required = ['phone', 'email'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($resp);
        }
        
        $email = $request->input('email');
        $phone = $request->input('phone');
        
        $userDb = User::where('email', $email)->where('phone', $phone)->first();
        if(empty($userDb)){
            $message = "User tidak ditemukan";
            $resp = ['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($resp);
        }
        
        $locker = Locker::where('id', $userDb->locker_id)->first();
        
        $response = ['response' => ['code' => 400, 'message' => ""], 'data' => []];
        
        if(isset($locker->locker_store_url) && !empty($locker->locker_store_url)){
            $url = $locker->locker_store_url.'/'.config('constant.locker.endpoint');
            $result = Api::callAPI('GET', $url, NULL);
            $response['response']['code'] = $result['code'];
            $response['response']['message'] = $result['message'];
        } else {
            Log::debug($locker->id.": locker_store_url is empty");
        }
        
        return $response;
    }
}
