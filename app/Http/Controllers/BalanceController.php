<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiPopWarung;
use App\Models\BalanceRecord;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\Transaction;
use App\Models\Payment;
use App\Http\Helpers\Helper;

class BalanceController extends Controller
{
    /**
     * balance record list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function balanceRecord(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' =>400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');

        // get data to db
        $balanceDb = BalanceRecord::listRecord($username);
        if (!$balanceDb->isSuccess){
            $message = $balanceDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $balanceDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $data];
        return response()->json($resp);
    }
    
    /**
     * Credit Deposit
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function creditDeposit(Request $request){
        
        try {
            
            $required = ['lockerid','totamount', 'transactionref'];
            $message = '';
            
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            
            // get username
            $transactionRef = $request->input('transactionref');
            $paymentid = $request->input('paymentid');
            $lockerId = $request->input('lockerid');
            $totalPrice = $request->input('totamount');
            $region_code = $request->input('regioncode');
            $username = $request->input('username');
//             $username = $request->input('username');
//             $reason = $request->input('reason');
//             $agentPassword = $request->input('agent_password');

//             $responses = $this->user_checker($username, $agentPassword);
//             if(!$responses->isSuccess){
//                 $message = $responses->errorMsg;
//                 $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
//                 return response()->json($resp);
            //             }
            
            $params = [
                'userid'            => $request->input('userid'),
                'username'          => $username,
                'lockerid'          => $lockerId,
                'regioncode'        => $region_code,
                'transactionref'    => $transactionRef,
                'date'              => $request->input('date'),
                'paymenttype'       => $request->input('paymenttype'),
                'paymentid'         => $paymentid,
                'description'       => $request->input('description'),
                'status'            => $request->input('status'),
                'totamount'         => $totalPrice,
                'customername'      => $request->input('customername'),
                'customeremail'     => $request->input('customeremail'),
                'create_date'       => date('Y-m-d H:i:s'),
                'update_date'       => date('Y-m-d H:i:s'),
                'items'             => $request->input('items'),
            ];
            
            $url = config('constant.popwarung.api_url').'/transaction/addtransactioncustomer';
            $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
            
            $api_response = json_decode($mresult, true);
            
            if($api_response['payload']['response']['code'] === 200){
                
                DB::beginTransaction();
                
                $transaction = Transaction::leftJoin('payment_non_va', 'payment_non_va.transaction_id', '=', 'transactions.reference')
                ->where('popbox_agent.payment_non_va.payment_id', '=', $paymentid)
                ->select('transactions.*')
                ->get();
                
//                 if(Helper::like_match('KIMONU%', strtoupper($region_code)) === false){
                    
//                     $depositDb = BalanceRecord::creditDeposit($lockerId, $totalPrice, $transaction[0]->id);
//                     if (!$depositDb->isSuccess){
//                         DB::rollback();
//                         throw new \Exception($depositDb->errorMsg);
//                     }
                    
//                 }
                
                $response = Payment::paidPaymentByNonVA($transaction[0]->reference, $username);
                if(!$response->isSuccess){
                    DB::rollback();
                    throw new \Exception($response->errorMsg);
                }
                
                DB::commit();
            }
            
            $resp = ['success' => true,
                'payload' => [
                    'response' => [
                        'code' => $api_response['payload']['response']['code'],
                        'message' => $api_response['payload']['response']['message']],
                    'data' => [$api_response['payload']['data']]]
            ];
            
            } catch (Exception $e) {
                
                \Log::error($e->getMessage());
                
                $resp = ['success' => false,
                    'payload' => [
                        'response' => [
                            'code' => 400,
                            'message' => $e->getMessage()],
                        'data' => []]
                ];
            }
            
            return response()->json($resp);
            
        }
        
        private function log($message){
            echo "$message\n";
            $location = storage_path()."/logs/";
            Log::logFile($location,'balance',"$message");
        }
}
