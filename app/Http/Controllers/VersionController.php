<?php

namespace App\Http\Controllers;

use App\Http\Helpers\WebCurl;
use App\User;
use App\Models\Version;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VersionController extends Controller
{
    var $curl;

    public function __construct() {
        $headers    = ['Content-Type: application/json'];
        $this->curl = new WebCurl($headers);
    }

    // function from old app
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;

    public function getLastVersion(Request $request)
    {
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }

        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        $mresultVersion = Version::orderBy('updated_at', 'desc')->first();

        $resp=['response' => ['code' => 200,'message' =>'OK'], 'data' => [$mresultVersion]];

        return response()->json($resp);
    }

    /**
     * ------------- End Public Function -------------
     */
}
