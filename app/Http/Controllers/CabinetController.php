<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Helpers\Api;
use App\Http\Helpers\ApiPopWarung;
use App\Http\Helpers\Helper;
use App\Models\BackendAccessLog;
use App\Models\Locker;
use App\Models\Cabinet;
use App\Http\Requests\SubmitStockOpname;
use App\Http\Requests\ListStockOpname;
use Illuminate\Support\Facades\Storage;
use App\Models\AuditTrails;
use Illuminate\Support\Facades\DB;

class CabinetController extends Controller
{   
    public function open(Request $request) {
        
        Log::debug("Init open rack");
        
        $required = ['cabinetId'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response=['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($response);
        }
        
        $message = "";
        $response = ['response' => ['code' => 400, 'message' => ""], 'data' => []];
        
        $cabinetId = $request->input('cabinetId');
        
        $phone = $request->input('phone');
        
        $insert = [
            "module" => $request->fullUrl(),
            "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
            "email" => $request->input('email'),
            "name" => $request->input('name'),
            "phone" => $phone,
            "request_json" => $request->toArray(),
            "response_json" => "",
            "created_at" => date('Y-m-d H:i:s')
        ];
        
        $locker = "";
        
        $cabinet = Cabinet::where("cabinet_id", $cabinetId)->where("status", "active")->first();
        
        if(empty($cabinet)){
            $response["response"]["message"] = "Rak tidak dikenal";
            return response()->json($response);
        } else if(!isset($cabinet->store_url) || (isset($cabinet->store_url) && empty($cabinet->store_url))){
            if($cabinet->is_white_code == 1) {
                $response['response']['code'] = 200;
                $response['response']['message'] = "success";
            } else {
                $response["response"]["message"] = "Data rak tidak dikenal";
                return response()->json($response);
            }
        }
        
        if(!empty($cabinet->store_url)){
            // check door status
//             $url = $cabinet->store_url.'/'.config('constant.locker.status');
//             $result = Api::callAPI('GET', $url, NULL);
//             if(isset($result['payload']) && !empty($result['payload'])){
//                 $payload = json_decode($result['payload'], true);
//                 $response['response']['code'] = $payload['code'];
//                 $response['response']['message'] = $payload['message'];
//                 if(isset($payload["status_door_open"]) && $payload["status_door_open"] < 1 //&& $cabinet->opened_by == $phone
//                     ){
//                     $response['response']['is_door_closed'] = false;
//                     return response()->json($response);
//                 } else {
//                     $cabinet->is_door_closed = $payload["status_door_open"];
//                     $cabinet->opened_by = null;
//                     $cabinet->save();
//                 }
//             }
            
            // open door
            $url = $cabinet->store_url.'/'.config('constant.locker.open');
            $result = Api::callAPI('GET', $url, NULL);
            if(isset($result['payload']) && !empty($result['payload'])){
                $payload = json_decode($result['payload'], true);
                $response['response']['code'] = "Rak berhasil dibuka" == $payload['message'] ? 200 : 500;
                $response['response']['message'] = $payload['message'];
            }
        } else {
            Log::debug($cabinet->lockerId.": store_url is empty");
        }
        
        if($response['response']['code'] = 200){
            
            $cabinet->is_door_closed = 0;
            $cabinet->opened_by = $phone;
            $cabinet->save();
            
            $region = \App\Models\Virtual\Locker::join("popbox_virtual.cities", "popbox_virtual.lockers.cities_id", "popbox_virtual.cities.id")
            ->join("popbox_virtual.regions", "popbox_virtual.regions.id", "popbox_virtual.cities.regions_id")
            ->where("popbox_virtual.lockers.locker_id", $cabinet->lockerId)
            ->select("popbox_virtual.regions.region_code", "popbox_virtual.regions.region_name", "popbox_virtual.lockers.locker_name")
            ->first();
            
            $locker = Locker::find($cabinet->lockerId);
            
            $response['data'][] = [
                "region_code" => $region->region_code,
                "region_name" => $region->region_name,
                "locker_id" => $locker->id,
                "locker_name" => $locker->locker_name,
            ];
        }
        
        $audit = BackendAccessLog::store($insert);
        
        $audit->response_json = json_encode($response);
        $audit->updated_at = date('Y-m-d H:i:s');
        $audit->save();
        
        Log::debug("End");
        
        return $response;
    }
    
    // List of transaction
    public function list(Request $request){
        
        $required = ['email','limit','start'];
        
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($resp);
        }
        
        $insert = [
            "module" => $request->fullUrl(),
            "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
            "email" => $request->input('email'),
            "name" => $request->input('name'),
            "phone" => $request->input('phone'),
            "request_json" => $request->toArray(),
            "response_json" => "",
            "created_at" => date('Y-m-d H:i:s')
        ];
        
        $audit = BackendAccessLog::store($insert);
        
        $startDate = '';
        $endDate = '';
        
        if (!empty($request->input('daterange'))) {
            $date_range = Helper::formatDateRange($request->input('daterange'), '/');
            $startDate = $date_range->startDate;
            $endDate = $date_range->endDate;
        }
        
        $params = [];
        $params['email']           = $request->input('email');
        $params['startdate']       = $startDate;
        $params['enddate']         = $endDate;
        $params['transactionid']   = $request->input('reference');
        $params['transactiontype'] = $request->input('transactiontype');
        $params['start']           = $request->input('start');
        $params['limit']           = $request->input('limit');;
        
        $url = config('constant.popwarung.api_url').'/transaction/selfservice/transaction-list';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        
        $api_response = json_decode($mresult, true);
        
        $rows = array();
        foreach($api_response['payload']['data'] as $row){
            $row['locker_name'] = Helper::findLocker($row['locker_id']);
            array_push($rows, $row);
        }
        
        $resp = ['response' => [
            'code' => $api_response['payload']['response']['code'], 
            'message' => $api_response['payload']['response']['message']],
            'count' => $api_response['payload']['count'], 
            'data' => $rows
        ];
        
        $audit->response_json = json_encode($resp);
        $audit->updated_at = date('Y-m-d H:i:s');
        $audit->save();
        
        return response()->json($resp);
        
    }
    
    public function findByParams(Request $request) {
        $required = ['barcode','region_code'];
        
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($resp);
        }
        
        $insert = [
            "module" => $request->fullUrl(),
            "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
            "email" => $request->input('email'),
            "name" => $request->input('name'),
            "phone" => $request->input('phone'),
            "request_json" => $request->toArray(),
            "response_json" => "",
            "created_at" => date('Y-m-d H:i:s')
        ];
        
        $audit = BackendAccessLog::store($insert);
        
        $params = [];
        $params['barcode']           = $request->input('barcode');
        $params['region_code']       = $request->input('region_code');
        
        $url = config('constant.popwarung.api_url').'/product/findByParams';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        
        $api_response = json_decode($mresult, true);
        
        $resp = ['response' => [
            'code' => $api_response['payload']['response']['code'],
            'message' => $api_response['payload']['response']['message']],
            'data' => $api_response['payload']['data']
        ];
        
        $audit->response_json = json_encode($resp);
        $audit->updated_at = date('Y-m-d H:i:s');
        $audit->save();
        
        return response()->json($resp);
    }
    
    public function summary(Request $request){
        
        $required = ['daterange','rev_pct'];
        
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400, 'message' => $message], 'data' => []];
            return response()->json($resp);
        }
        
        $startDate = '';
        $endDate = '';
        
        if (!empty($request->input('daterange'))) {
            $date_range = Helper::formatDateRange($request->input('daterange'), '/');
            $startDate = $date_range->startDate;
            $endDate = $date_range->endDate;
        }
        
        $params = [];
        $params['email']        = $request->input('email');
        $params['startdate']    = $startDate;
        $params['enddate']      = $endDate;
        $params['locker_id']    = $request->input('locker_id');
        $params['rev_pct']      = $request->input('rev_pct');
        $params['payment_type'] = $request->input('payment_type');
        
        $url = config('constant.popwarung.api_url').'/transaction/selfservice/summarizes';
        $mresult = ApiPopWarung::call('POST', $url, json_encode($params));
        
        $api_response = json_decode($mresult, true);
        
        $resp = ['response' => [
            'code' => $api_response['payload']['response']['code'],
            'message' => $api_response['payload']['response']['message']],
            'data' => $api_response['payload']['data']
        ];
        
        return response()->json($resp);
        
    }

    public function listStockOpname(ListStockOpname $request){
        
        $resp = [
            'response' => [
                'code' => 400,
                'message' => "Failed to populate histories of stock opname"
            ],
            'size' => 0,
            'data' => []
        ];
        
        try {
            
            $insert = [
                "module" => $request->fullUrl(),
                "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
                "email" => $request->get('email'),
                "name" => $request->get('name'),
                "phone" => $request->get('phone'),
                "request_json" => json_encode($request->toArray()),
                "response_json" => "",
                "created_at" => date('Y-m-d H:i:s')
            ];
            
            $log = BackendAccessLog::store($insert);
            
            $start_date =  $request->get("start_date");
            $end_date = $request->get("end_date");
            
            $result = AuditTrails::whereBetween(DB::raw('DATE(created_at)'), array($start_date, $end_date));
            
            $size = clone $result;
            
            $result = $result->offset($request->get('start', 0))->limit($request->get('limit', 10))
            ->select("json_after", "created_at")
            ->orderBy("created_at", "DESC")
            ->get();
            
            $rows = [];
            foreach ($result as $r) {
                
                $r = json_decode($r, true);
                $created_at = $r["created_at"];
                $r = json_decode($r["json_after"], true);
                $warung_id = $r["locker_id"];
                $row = [];
                
                $warung = Locker::find($warung_id);
                
                $row["warung_id"] = $warung_id;
                $row["warung_name"] = $warung["locker_name"];
                $row["created_at"] = $created_at;
                
                foreach ($r["items"] as $item){
                    if(!isset($item["stock_opname"])) {
                        $item["stock_opname"] = $item["max_capacity"] - $item["input_stock"];
                    }
                    unset($item["max_capacity"]);
                    unset($item["current_stock"]);
                    unset($item["suggested_stock"]);
                    unset($item["updated_by"]);
                    
                    $row["products"][] = $item;
                }
                
                $row["image_before"] = "";
                $row["image_after"] = "";
                if(isset($r["file_info"])){
                    foreach ($r["file_info"] as $file){
                        $fraction = $file["taken"] == "before" ? "old" : "latest";
                        
                        $row["image_".$file["taken"]] = config("constant.agent_url")."stockopname/$fraction"."_".$file["filename"];
                    }
                }
                
                $rows[] = $row;
            }
            if(!empty($rows)){
                $resp['response']['code'] = 200;
                $resp['response']['message'] = "Populate: success";
                $resp['size'] = $size->count();
                $resp['data'] = $rows;
            }
            
            $log->response_json = json_encode($resp);
            $log->updated_at = date('Y-m-d H:i:s');
            $log->save();
        } catch (\Exception $e) {
            Log::error(get_class($e)." line #".$e->getLine().": ".$e->getMessage());
        }
        
        return response()->json($resp);
    }
    
    public function submitStockOpname(SubmitStockOpname $request){
        
        $resp = [
            'response' => [
                'code' => 400,
                'message' => "Failed to submit stock opname"
            ],
            'data' => []
        ];
        
        try {
            
            $request_param = [
                "warung_id" => $request->get("warung_id"),
                "items"     => $request->get("items"),
            ];
            
            $images = request("images");
            
            $file_info = [];
            foreach ($images as $image){
                
                $fraction = $image["taken"] == "before" ? "old" : "latest";
                
                Storage::disk("stockopname")->put($fraction."_".$image["filename"], base64_decode($image["file"]));
                
                unset($image["file"]);
                
                $file_info[] = $image;
            }
            
            $request_param["file_info"] = $file_info;
            
            $insert = [
                "module" => $request->fullUrl(),
                "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
                "email" => $request->input('email'),
                "name" => $request->input('name'),
                "phone" => $request->input('phone'),
                "request_json" => json_encode($request_param),
                "response_json" => "",
                "created_at" => date('Y-m-d H:i:s')
            ];
            
            $log = BackendAccessLog::store($insert);
            
            unset($request_param["file_info"]);
            $url = config('constant.popwarung.api_url').'/product/update';
            $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($request_param));
            
            $api_response = json_decode($mresult, true);
            
            $resp['response']['code'] = $api_response['success'] == true ? 200 : 400;
            $resp['response']['message'] = $api_response['payload']['message'];
            $resp['data'] = $api_response['payload']['data'];
            
            $log->response_json = json_encode($resp);
            $log->updated_at = date('Y-m-d H:i:s');
            $log->save();
            
            $json_after = str_replace("warung_id", "locker_id", $insert["request_json"]);
            $audit = new AuditTrails();
            $audit->key = $request->get("warung_id");
            $audit->module = "stock_replenishment";
            $audit->type = "add";
            $audit->json_before = json_encode([]);
            $audit->json_after = $json_after;
            $audit->created_by = $request->get("created_by");
            $audit->save();
            
        } catch (\Exception $e) {
            Log::error(get_class($e)." line #".$e->getLine().": ".$e->getMessage());
        }
        
        return response()->json($resp);
    }
}
