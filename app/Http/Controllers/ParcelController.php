<?php

namespace App\Http\Controllers;

use App\Http\Helpers\APIv2;
use App\Http\Helpers\Helper;
use App\Http\Helpers\WebCurl;
use App\Models\Locker;
use App\Models\Payment;
use App\Models\SmsNotification;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\VirtualLocker;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ParcelController extends Controller
{
    var $curl;

    public function __construct() {
        $headers    = ['Content-Type: application/json'];
        $this->curl = new WebCurl($headers);
    }

    // function from old app
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCity(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get province data first
        $apiv2 = new APIv2();
        $result = $apiv2->getProvincePickupData();
        if (empty($result)){
            $message = 'Failed to get Pulsa Product';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $dataResult = [];

        $provinceResult = $result->data;
        foreach ($provinceResult as $result){
            $provinceName = $result->province;
            $provTmp = new \stdClass();
            $provTmp->province_name = $result->province;
            $cityList = [];
            // get city data
            $result = $apiv2->getCityPickupData($provinceName);
            if (empty($result)){ continue; }
            if ($result->response->code!=200){ continue; }

            $cityResult = $result->data->city;
            foreach ($cityResult as $item) {
                if ($item->name == 'South Jakarta City') continue;
                $cityTmp = new \stdClass();
                $cityTmp->city_name = $item->name;
                $districtList = [];
                foreach ($item->kecamatan as $district){
                    $districtTmp = new \stdClass();
                    $districtTmp->district_name = $district;
                    $districtList[] = $districtTmp;
                }
                $cityTmp->district = $districtList;
                $cityList[] = $cityTmp;
            }
            $provTmp->cities = $cityList;
            $dataResult[] = $provTmp;
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $dataResult];
        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderCalculation(Request $request){
        $required = ['destination_type','parcel_size'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $destinationType = $request->input('destination_type','address');
        // create validation for address
        if ($destinationType == 'address'){
            $required = ['destination_city','destination_district'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        // type locker
        elseif ($destinationType == 'locker'){
            $required = ['locker_id'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        // service not found
        else {
            $message = "Invalid Destination Type";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // calculate
        $totalPrice = 0;
        $lockerName = null;
        $lockerAddress = null;
        $estimatedTime = null;
        if ($destinationType == 'address'){
            $response = $this->calculateAddress($input);
            if (!$response->isSuccess){
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
            $estimatedTime = $response->estimatedTime;
        }
        // calculate locker
        else {
            $response = $this->calculateLocker($input);
            if (!$response->isSuccess){
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
            $lockerName = $response->lockerName;
            $lockerAddress = $response->lockerAddress;
            $estimatedTime = $response->estimatedTime;
        }

        // create response
        $result = new \stdClass();
        $result->invoice_id = null;
        $result->sender_name = $request->input('sender_name',null);
        $result->sender_phone = $request->input('sender_phone',null);
        $result->destination_name = $request->input('destination_name',null);
        $result->destination_phone = $request->input('destination_phone',null);
        $result->parcel_size = $request->input('parcel_size');
        $result->parcel_description = $request->input('parcel_description');

        $result->destination_type = $request->input('destination_type');
        $result->destination_address = $request->input('destination_address',null);
        $result->destination_city = $request->input('destination_city',null);
        $result->destination_district = $request->input('destination_district',null);
        $result->locker_name = $lockerName;
        $result->locker_address = $lockerAddress;
        $result->estimated_time = $estimatedTime;
        $result->total_price = (int)$totalPrice;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$result]];
        return response()->json($resp);
    }

    /**
     * Submit PopSend
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postOrderSubmit(Request $request){
        $required = ['sender_name','sender_phone','destination_name','destination_phone','destination_type','parcel_size','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $destinationType = $request->input('destination_type','address');
        $agentPassword = $request->input('agent_password');
        // create validation for address
        if ($destinationType == 'address'){
            $required = ['destination_city','destination_district','destination_address'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        // type locker
        elseif ($destinationType == 'locker'){
            $required = ['locker_id'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        // service not found
        else {
            $message = "Invalid Destination Type";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        DB::beginTransaction();

        // Calculate Price
        $totalPrice = 0;
        if ($destinationType == 'address'){
            $response = $this->calculateAddress($input);
            if (!$response->isSuccess){
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
        }
        // calculate locker
        else {
            $response = $this->calculateLocker($input);
            if (!$response->isSuccess){
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
        }
        $username = $request->input('username');
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;

        $senderName = $request->input('sender_name');
        $itemName = "Kirim Barang $senderName";
        $itemType = 'delivery';
        $itemPrice = $totalPrice;
        $itemParam = $input;
        $itemPicture = null;

        unset($itemParam['agent_password']);
        unset($itemParam['username']);

        // create transaction
        $transactionDb = Transaction::createSingleTransaction($username,$itemName,$itemType,$itemPrice,$itemParam,$itemPicture);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // submit for popsend
        $totalPrice = 0;
        $lockerName = null;
        $lockerAddress = null;
        $invoiceId = null;
        if ($destinationType == 'address'){
            $response = $this->submitAddress($input);
            if (!$response->isSuccess){
                DB::rollback();
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
            $invoiceId = $response->invoiceId;
        }
        // calculate locker
        else {
            $response = $this->submitLocker($input);
            if (!$response->isSuccess){
                DB::rollback();
                $message = $response->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $totalPrice = $response->price;
            $lockerName = $response->lockerName;
            $lockerAddress = $response->lockerAddress;
            $invoiceId = $response->invoiceId;
        }

        // push to locker
        // create params
        $params = [];
        $params['username'] = "agent_".$username;
        $params['locker_id'] = $lockerId;
        $params['service'] = "popsend";
        $params['order_id'] = $invoiceId;
        $params['phone'] = $request->input('sender_phone');
        $params['email'] = null;
        $params['description'] = $request->input('parcel_description');
        $params['destination_name'] = $request->input('destination_name');
        $params['destination_phone'] = $request->input('destination_phone');
        $params['status'] = 'CUSTOMER_STORE';

        $url = 'parcel/create';
        $postApi = $this->postApi($url,$params);
        $postApi = json_decode($postApi);

        if (!$postApi || $postApi->response->code!=200){
            DB::rollback();
            $message =  $postApi->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // update transaction Item
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        foreach ($transactionDb->items as $item){
            $itemDb = TransactionItem::find($item->id);
            $itemDb->status_push = 'COMPLETED';
            $itemDb->reference = $invoiceId;
            $itemDb->save();
        }
        $username = $input['username'];
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;
        $lockerDb = VirtualLocker::where('locker_id',$lockerId)->first();

        $insert = [];
        $insert['id'] = 'agent'.date('Ymd').Helper::generateRandomString(20);
        $insert['tracking_no'] = $invoiceId;
        $insert['phone_number'] = $request->input('destination_phone',null);
        $insert['locker_name'] = $lockerDb->type." ".$lockerDb->locker_name;
        $insert['locker_size'] = 'S';
        $insert['locker_number'] = $lockerDb->id;
        $insert['storetime'] = date('Y-m-d H:i:s');
        $insert['taketime'] = "0000-00-00 00:00:00";
        $insert['status'] = 'IN_STORE';
        // insert  into locker activities pickup
        $lockerActivitiesPickupDb = DB::connection('popbox_db')
            ->table('locker_activities_pickup')
            ->insert($insert);
        DB::commit();

        // send sms notification for pickup
        $environment = App::environment();
        $message = "Ada pick up request baru dengan dari Agent $lockerDb->locker_name no invoice $invoiceId silakan diproses melalui dashboard.";

        if ($environment=='production'){
            $adminSmsDb = DB::connection('popbox_db')
                ->table('admin_notification')
                ->select('admin_phone')
                ->get();
            foreach ($adminSmsDb as $item) {
                // insert to sms notification PIN for destination if available or sender
                $smsNotif = new SmsNotification();
                $smsNotif->createSMS('create_popsend','nexmo',$item->admin_phone,$message);
            }
        } else {
            $smsNotif = new SmsNotification();
            $smsNotif->createSMS('create_popsend','nexmo','08112633808',$message);
        }

        // create response
        $result = new \stdClass();
        $result->invoice_id = $invoiceId;
        $result->sender_name = $request->input('sender_name',null);
        $result->sender_phone = $request->input('sender_phone',null);
        $result->destination_name = $request->input('destination_name',null);
        $result->destination_phone = $request->input('destination_phone',null);
        $result->parcel_size = $request->input('parcel_size');
        $result->parcel_description = $request->input('parcel_description');

        $result->destination_type = $request->input('destination_type');
        $result->destination_address = $request->input('destination_address',null);
        $result->destination_city = $request->input('destination_city',null);
        $result->destination_district = $request->input('destination_district',null);
        $result->locker_name = $lockerName;
        $result->locker_address = $lockerAddress;
        $result->total_price = (int)$totalPrice;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$result]];
        return response()->json($resp);
    }

    /**
     * Calculate Address
     * @param $input
     * @return \stdClass
     */
    private function calculateAddress($input){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->price = null;
        $response->estimatedTime = null;

        // get locker data
        $username = $input['username'];
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;
        $lockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
        $pickupAddressDetail = $lockerDb->address." ".$lockerDb->detail_address;
        $pickupAddressLat = $lockerDb->latitude;
        $pickupAddressLong = $lockerDb->longitude;
        $pickupLockerName = strtoupper($lockerDb->type)." ".$lockerDb->locker_name;
        $pickupLockerSize = strtoupper(empty($input['parcel_size']) ? 'S' : $input['parcel_size']);
        $pickupDate = date('Y-m-d');
        $pickupTime = date('H:i:s');
        $itemDescription = empty($input['parcel_description']) ? null : $input['parcel_description'];
        $promoCode = empty($input['promo_code']) ? null : $input['promo_code'];
        $recipientName = empty($input['destination_name']) ? null : $input['destination_name'];
        $recipientPhone = empty($input['destination_phone']) ? null : $input['destination_phone'];
        $recipientAddress = empty($input['destination_address']) ? null : $input['destination_address'];
        $recipientAddressCity = $input['destination_city'];
        $recipientAddressKec = $input['destination_district'];

        // create param
        $params = [];
        $params['phone'] = $username;
        $params['pickup_address'] = null;
        $params['pickup_address_detail'] = $pickupAddressDetail;
        $params['pickup_address_lat'] = $pickupAddressLat;
        $params['pickup_address_long'] = $pickupAddressLong;
        $params['pickup_locker_name'] = $pickupLockerName;
        $params['pickup_locker_size'] = $pickupLockerSize;
        $params['pickup_date'] = $pickupDate;
        $params['pickup_time'] = $pickupTime;
        $params['item_description'] = $itemDescription;
        $params['promo_code'] = $promoCode;
        $params['recipient_name'] = $recipientName;
        $params['recipient_phone'] = $recipientPhone;
        $params['recipient_address'] = $recipientAddress;
        $params['recipient_address_detail'] = null;
        $params['recipient_address_city'] = $recipientAddressCity;
        $params['recipient_address_kec'] = $recipientAddressKec;
        $params['recipient_address_lat'] = null;
        $params['recipient_address_long'] = null;
        $params['recipient_locker_name'] = null;
        $params['recipient_locker_size'] = null;
        $params['recipient_email'] = null;
        $params['item_photo_1'] = null;

        // push calculation to api
        $apiv2 = new APIv2();
        $result = $apiv2->popsendCalculation($params);
        if (empty($result)){
            $response->errorMsg = "Failed to Get Calculation";
            return $response;
        }
        if ($result->response->code!=200){
            $response->errorMsg = $result->response->message;
            return $response;
        }
        $data = $result->data[0];
        $amount = $data->amount;
        $response->price = $amount;
        $response->estimatedTime = $data->est_delivery;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * @param $input
     * @return \stdClass
     */
    private function calculateLocker($input){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->price = null;
        $response->lockerName = null;
        $response->lockerAddress = null;
        $response->estimatedTime = null;

        // get locker data
        $username = $input['username'];
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;
        $lockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
        $pickupAddressDetail = $lockerDb->address." ".$lockerDb->detail_address;
        $pickupAddressLat = $lockerDb->latitude;
        $pickupAddressLong = $lockerDb->longitude;
        $pickupLockerName = strtoupper($lockerDb->type)." ".$lockerDb->locker_name;
        $pickupLockerSize = strtoupper(empty($input['parcel_size']) ? 'S' : $input['parcel_size']);
        $pickupDate = date('Y-m-d');
        $pickupTime = date('H:i:s');
        $itemDescription = empty($input['parcel_description']) ? null : $input['parcel_description'];
        $promoCode = empty($input['promo_code']) ? null : $input['promo_code'];
        $recipientName = empty($input['destination_name']) ? null : $input['destination_name'];
        $recipientPhone = empty($input['destination_phone']) ? null : $input['destination_phone'];
        // get locker destination
        $destinationLockerId = $input['locker_id'];
        $destinationLockerDb = VirtualLocker::where('locker_id',$destinationLockerId)->first();
        $recipientLockerName = strtoupper($destinationLockerDb->type)." ".$destinationLockerDb->locker_name;
        $recipientAddressLat = $destinationLockerDb->latitude;
        $recipientAddressLong = $destinationLockerDb->longitude;

        // create param
        $params = [];
        $params['phone'] = $username;
        $params['pickup_address'] = null;
        $params['pickup_address_detail'] = $pickupAddressDetail;
        $params['pickup_address_lat'] = $pickupAddressLat;
        $params['pickup_address_long'] = $pickupAddressLong;
        $params['pickup_locker_name'] = $pickupLockerName;
        $params['pickup_locker_size'] = $pickupLockerSize;
        $params['pickup_date'] = $pickupDate;
        $params['pickup_time'] = $pickupTime;
        $params['item_description'] = $itemDescription;
        $params['promo_code'] = $promoCode;
        $params['recipient_name'] = $recipientName;
        $params['recipient_phone'] = $recipientPhone;
        $params['recipient_address'] = null;
        $params['recipient_address_detail'] = null;
        $params['recipient_address_city'] = null;
        $params['recipient_address_kec'] = null;
        $params['recipient_address_lat'] = $recipientAddressLat;
        $params['recipient_address_long'] = $recipientAddressLong;
        $params['recipient_locker_name'] = $recipientLockerName;
        $params['recipient_locker_size'] = null;
        $params['recipient_email'] = null;
        $params['item_photo_1'] = null;

        // push calculation to api
        $apiv2 = new APIv2();
        $result = $apiv2->popsendCalculation($params);
        if (empty($result)){
            $response->errorMsg = "Failed to Get Calculation";
            return $response;
        }
        if ($result->response->code!=200){
            $response->errorMsg = $result->response->message;
            return $response;
        }
        $data = $result->data[0];
        $amount = $data->amount;
        $response->price = $amount;
        $response->isSuccess = true;
        $response->lockerName = $recipientLockerName;
        $response->lockerAddress = $destinationLockerDb->address;
        $response->estimatedTime = $data->est_delivery;
        return $response;
    }

    /**
     * Submit Locker to Address
     * @param $input
     * @return \stdClass
     */
    private function submitAddress($input){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->price = null;

        // get locker data
        $username = $input['username'];
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;
        $lockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
        $pickupAddressDetail = $lockerDb->address." ".$lockerDb->detail_address;
        $pickupAddressLat = $lockerDb->latitude;
        $pickupAddressLong = $lockerDb->longitude;
        $pickupLockerName = strtoupper($lockerDb->type)." ".$lockerDb->locker_name;
        $pickupLockerSize = strtoupper(empty($input['parcel_size']) ? 'S' : $input['parcel_size']);
        $pickupDate = date('Y-m-d');
        $pickupTime = date('H:i:s');
        $itemDescription = empty($input['parcel_description']) ? null : $input['parcel_description'];
        $promoCode = empty($input['promo_code']) ? null : $input['promo_code'];
        $recipientName = empty($input['destination_name']) ? null : $input['destination_name'];
        $recipientPhone = empty($input['destination_phone']) ? null : $input['destination_phone'];
        $recipientAddress = $input['destination_address'];
        $recipientAddressCity = $input['destination_city'];
        $recipientAddressKec = $input['destination_district'];

        // create param
        $params = [];
        $params['phone'] = $username;
        $params['pickup_address'] = null;
        $params['pickup_address_detail'] = $pickupAddressDetail;
        $params['pickup_address_lat'] = $pickupAddressLat;
        $params['pickup_address_long'] = $pickupAddressLong;
        $params['pickup_locker_name'] = $pickupLockerName;
        $params['pickup_locker_size'] = $pickupLockerSize;
        $params['pickup_date'] = $pickupDate;
        $params['pickup_time'] = $pickupTime;
        $params['item_description'] = $itemDescription;
        $params['promo_code'] = $promoCode;
        $params['recipient_name'] = $recipientName;
        $params['recipient_phone'] = $recipientPhone;
        $params['recipient_address'] = $recipientAddress;
        $params['recipient_address_detail'] = null;
        $params['recipient_address_city'] = $recipientAddressCity;
        $params['recipient_address_kec'] = $recipientAddressKec;
        $params['recipient_address_lat'] = null;
        $params['recipient_address_long'] = null;
        $params['recipient_locker_name'] = null;
        $params['recipient_locker_size'] = null;
        $params['recipient_email'] = null;
        $params['item_photo_1'] = null;

        // push calculation to api
        $apiv2 = new APIv2();
        $result = $apiv2->popsendSubmit($params);
        if (empty($result)){
            $response->errorMsg = "Failed to Post Submit";
            return $response;
        }
        if ($result->response->code!=200){
            $response->errorMsg = $result->response->message;
            return $response;
        }
        $data = $result->data[0];
        $amount = $data->total_amount;
        $response->price = $amount;
        $response->isSuccess = true;
        $response->invoiceId = $data->invoice_id;
        return $response;
    }

    /**
     * Submit Locker to Locker
     * @param $input
     * @return \stdClass
     */
    private function submitLocker($input){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->price = null;
        $response->lockerName = null;
        $response->lockerAddress = null;

        // get locker data
        $username = $input['username'];
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;
        $lockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
        $pickupAddressDetail = $lockerDb->address." ".$lockerDb->detail_address;
        $pickupAddressLat = $lockerDb->latitude;
        $pickupAddressLong = $lockerDb->longitude;
        $pickupLockerName = strtoupper($lockerDb->type)." ".$lockerDb->locker_name;
        $pickupLockerSize = strtoupper(empty($input['parcel_size']) ? 'S' : $input['parcel_size']);
        $pickupDate = date('Y-m-d');
        $pickupTime = date('H:i:s');
        $itemDescription = empty($input['parcel_description']) ? null : $input['parcel_description'];
        $promoCode = empty($input['promo_code']) ? null : $input['promo_code'];
        $recipientName = empty($input['destination_name']) ? null : $input['destination_name'];
        $recipientPhone = empty($input['destination_phone']) ? null : $input['destination_phone'];
        // get locker destination
        $destinationLockerId = $input['locker_id'];
        $destinationLockerDb = VirtualLocker::where('locker_id',$destinationLockerId)->first();
        $recipientLockerName = strtoupper($destinationLockerDb->type)." ".$destinationLockerDb->locker_name;
        $recipientAddressLat = $destinationLockerDb->latitude;
        $recipientAddressLong = $destinationLockerDb->longitude;

        // create param
        $params = [];
        $params['phone'] = $username;
        $params['pickup_address'] = null;
        $params['pickup_address_detail'] = $pickupAddressDetail;
        $params['pickup_address_lat'] = $pickupAddressLat;
        $params['pickup_address_long'] = $pickupAddressLong;
        $params['pickup_locker_name'] = $pickupLockerName;
        $params['pickup_locker_size'] = $pickupLockerSize;
        $params['pickup_date'] = $pickupDate;
        $params['pickup_time'] = $pickupTime;
        $params['item_description'] = $itemDescription;
        $params['promo_code'] = $promoCode;
        $params['recipient_name'] = $recipientName;
        $params['recipient_phone'] = $recipientPhone;
        $params['recipient_address'] = null;
        $params['recipient_address_detail'] = null;
        $params['recipient_address_city'] = null;
        $params['recipient_address_kec'] = null;
        $params['recipient_address_lat'] = $recipientAddressLat;
        $params['recipient_address_long'] = $recipientAddressLong;
        $params['recipient_locker_name'] = $recipientLockerName;
        $params['recipient_locker_size'] = null;
        $params['recipient_email'] = null;
        $params['item_photo_1'] = null;

        // push calculation to api
        $apiv2 = new APIv2();
        $result = $apiv2->popsendSubmit($params);
        if (empty($result)){
            $response->errorMsg = "Failed to Post Submit";
            return $response;
        }
        if ($result->response->code!=200){
            $response->errorMsg = $result->response->message;
            return $response;
        }
        $data = $result->data[0];
        $amount = $data->total_amount;
        $response->isSuccess = true;
        $response->price = $amount;
        $response->lockerName = $recipientLockerName;
        $response->lockerAddress = $destinationLockerDb->address;
        $response->invoiceId = $data->invoice_id;
        return $response;
    }

    /**
     * @param $url
     * @param $params
     * @return mixed
     */
    private function postApi($url,$params){
        $url = env('VL_URL','http://locker.dev/api/').$url;
        $params['token'] = env('VL_TOKEN','79XM983RH8TK37KTR84NNUCYK8DTLTRDXE5Z77UP');

        $headers = array (
            'Content-Type: application/json'
        );
        $result = null;
        $curl = curl_init ();
        $params = json_encode($params);

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLINFO_HEADER_OUT, true);
        curl_setopt ( $curl, CURLOPT_POST, 1 );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $params );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );

        curl_close ( $curl );

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $params,
                'api_response'  => $content,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $content;
    }
}
