<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiPayment;
use App\Http\Helpers\APIv2;
use App\Models\LockerVAOpen;
use App\Models\Payment;
use App\Models\PaymentDokuVA;
use App\Models\PaymentMethod;
use App\Models\PaymentTransfer;
use App\Models\PaymentVirtualAccount;
use App\Models\PaymentNonVA;
use App\Models\Transaction;
use App\Models\VirtualLocker;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class TopUpController extends Controller
{
    /**
     * Get Payment Method for Top Up
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTopUpMethod(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $phone = $request->input('username');
        //get user db
        $userDb = User::where('username',$phone)->first();
        $lockerId = $userDb->locker_id;

        // get payment method available
        $paymentMethodDb = PaymentMethod::join('payment_available_module','payment_available_module.payment_methods_id','payment_methods.id')
            ->where('module','topup')
            ->get();
        // create response
        $paymentMethodList = [];
        foreach ($paymentMethodDb as $item){
            if ($item->status == 'OFF') continue;
            // find VA OPEN
            $isAvailable = false;
            $vaNumber = null;
            $lockerVAOpen = LockerVAOpen::where('lockers_id',$lockerId)
                ->where('va_type',$item->code)
                ->first();
            if ($lockerVAOpen){
                $isAvailable = true;
                $vaNumber = $lockerVAOpen->va_number;
            }

            $tmp = new \stdClass();
            $tmp->module = $item->module;
            $tmp->code = $item->code;
            $tmp->name = $item->name;
            $tmp->status = $item->status;
            $tmp->is_open_available = $isAvailable;
            $tmp->va_open_number = $vaNumber;
            $paymentMethodList[] = $tmp;
        }
        if (empty($paymentMethodList)){
            $message = "Payment Method Not Available";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $paymentMethodList];
        return response()->json($resp);
    }

    /**
     * Get Available Bank Transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBankDestination(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $availableBanks = [];
        $availableBanks = PaymentTransfer::getAvailableBanks();

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $availableBanks];
        return response()->json($resp);
    }

    /**
     * Create Top Up Request with Manual Bank Transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBankTransfer(Request $request){
        $required = ['amount','bank_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // validate
        $rules = [
            'amount' => 'required|integer'
        ];
        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $amount = $input['amount'];
        if ($amount<100000){
            $message = "Minimum Top Up 100.000";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $input['username'];
        $bankId = $input['bank_id'];

        DB::beginTransaction();
        // create top up transaction
        $transactionDb = Transaction::createTopUpTransaction($username,$amount);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;
        $method = 'transfer';

        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // insert payment transfer
        $paymentTransferDb = PaymentTransfer::createPaymentTransfer($paymentDb->paymentId,$bankId);
        if (!$paymentTransferDb->isSuccess){
            DB::rollback();
            $message = $paymentTransferDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();
        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);

    }

    /**
     * Submit form confirmation
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmBankTransfer(Request $request){
        $required = ['reference','sender_bank','sender_name'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $transactionRef = $request->input('reference');
        $username = $request->input('username');
        DB::beginTransaction();

        // check transaction number
        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        if (!$transactionDb){
            $message = "Transaction Not Exist";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // insert confirmation
        $paymentTransferDb = PaymentTransfer::submitConfirmation($transactionRef,$input);
        if (!$paymentTransferDb->isSuccess){
            DB::rollback();
            $message = $paymentTransferDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;
        $environment = App::environment();
        if ($environment == 'production'){
            Mail::send('email.topup.pending-confirmation', ['data' => $data], function ($m) use ($data) {
                $m->from('no-reply@popbox.asia', 'PopBox Asia Agent');
                $m->to('lidya@popbox.asia', 'PopBox Finance')->subject("[PopBox Agent] Bank Transfer Pending :$data->reference");
                $m->to('greta@popbox.asia', 'PopBox Finance')->subject("[PopBox Agent] Bank Transfer Pending :$data->reference");
                $m->cc('it@popbox.asia', 'PopBox Finance')->subject("[PopBox Agent] Bank Transfer Pending :$data->reference");
            });
        } else {
            Mail::send('email.topup.pending-confirmation', ['data' => $data], function ($m) use ($data) {
                $m->from('no-reply@popbox.asia', 'PopBox Asia Agent');
                $m->to('arief@popbox.asia', 'PopBox Finance')->subject("[PopBox Agent] Testing Bank Transfer Pending :$data->reference");
            });
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Deprecated
     * create Doku VA Transfer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDokuVATransfer(Request $request){
        $required = ['amount','device_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $deviceId = $request->input('device_id');
        $amount = $request->input('amount');

        $userDb = User::where('username',$username)->first();
        $lockerDb = VirtualLocker::where('locker_id',$userDb->locker_id)->first();
        $expiredMinute = 720;

        DB::beginTransaction();
        $method = 'doku_va';

        $transactionDb = Transaction::createTopUpTransaction($username,$amount,$method);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;

        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $totalAmount = $transactionDb->transactionAmount;
        $timeLimit = date('Y-m-d H:i:s',strtotime("+24 hours"));
        // create param
        $param = [];
        $param['amount'] = $totalAmount;
        $param['billing_type'] = 'fixed';
        $param['transaction_id'] = $transactionRef;
        $param['customer_name'] = $lockerDb->locker_name;
        $param['customer_email'] = $userDb->email;
        $param['customer_phone'] = $userDb->phone;
        $param['description'] = "Top Up Agent ".$lockerDb->locker_name."Rp ".$totalAmount;
        // set expired time
        $param['datetime_expired'] = $timeLimit;
        $param['method_code'] = 'DOKU-PERMATA';

        // post to API
        $apiV2 = new ApiPayment();
        $result =  $apiV2->createPayment($param);

        if (empty($result)){
            DB::rollback();
            $message = "Failed to Push Doku API";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!=200){
            DB::rollback();
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $result->data[0];

        $vaNumber = $data->virtual_account_number;
        $expiredDate = $data->datetime_expired;
        $paymentId = $data->payment_id;

        $permataVA =  $data->virtual_account_number;

        // insert payment transfer
        $paymentTransferDb = PaymentDokuVA::createPaymentDokuVA($paymentDb->paymentId,$method,$permataVA,'WAITING');
        if (!$paymentTransferDb->isSuccess){
            DB::rollback();
            $message = 'Failed to create Payment Doku VA';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();
        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * Create Top Up with Virtual Account
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createVirtualAccount(Request $request){
        $required = ['amount','device_id','payment_method'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $deviceId = $request->input('device_id');
        $amount = $request->input('amount');
        $paymentMethodCode = $request->input('payment_method');

        $userDb = User::where('username',$username)->first();
        $lockerDb = VirtualLocker::where('locker_id',$userDb->locker_id)->first();
        $expiredMinute = 24;

        DB::beginTransaction();

        $transactionDb = Transaction::createTopUpTransaction($username,$amount,$paymentMethodCode);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;
        $timeLimit = date('Y-m-d H:i:s',strtotime("+$expiredMinute hours"));
        $paymentDb = Payment::addPayment($transactionRef,$username,$paymentMethodCode,$timeLimit);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $totalAmount = $transactionDb->transactionAmount;
        // create param
        $param = [];

        $param['amount'] = $totalAmount;
        $param['billing_type'] = 'fixed';
        $param['transaction_id'] = $transactionRef;
        $param['customer_name'] = $lockerDb->locker_name;
        $param['customer_email'] = $userDb->email;
        $param['customer_phone'] = $userDb->phone;
        $param['description'] = "Top Up Agent ".$lockerDb->locker_name."Rp ".$totalAmount;
        // set expired time
        $param['datetime_expired'] = $timeLimit;

        $isSuccess = false;
        $message = null;

        switch ($paymentMethodCode){
            case 'doku_mandiri_va' :
                $param['method_code'] = 'DOKU-MANDIRI';
                // post to API
                $apiV2 = new ApiPayment();
                $result =  $apiV2->createPayment($param);

                if (empty($result)){
                    $message = "Failed to Push Payment API";
                    break;
                }
                if ($result->response->code!=200){
                    $message = $result->response->message;
                    break;
                }
                $data = $result->data[0];

                $vaNumber = $data->virtual_account_number;
                $expiredDate = $data->datetime_expired;
                $paymentId = $data->payment_id;
                // insert payment transfer
                $paymentVADb = PaymentVirtualAccount::createPayment($paymentDb->paymentId,$paymentId,'fixed',$vaNumber,'WAITING');
                if (!$paymentVADb->isSuccess){
                    $message = 'Failed to create Payment Doku VA';
                    break;
                }
                $isSuccess = true;
                break;
            case 'doku_permata_va' :
                $param['method_code'] = 'DOKU-PERMATA';
                // post to API
                $apiV2 = new ApiPayment();
                $result =  $apiV2->createPayment($param);

                if (empty($result)){
                    $message = "Failed to Push Payment API";
                    break;
                }
                if ($result->response->code!=200){
                    $message = $result->response->message;
                    break;
                }
                $data = $result->data[0];

                $vaNumber = $data->virtual_account_number;
                $expiredDate = $data->datetime_expired;
                $paymentId = $data->payment_id;
                // insert payment transfer
                $paymentVADb = PaymentVirtualAccount::createPayment($paymentDb->paymentId,$paymentId,'fixed',$vaNumber,'WAITING');
                if (!$paymentVADb->isSuccess){
                    $message = 'Failed to create Payment Doku VA';
                    break;
                }
                $isSuccess = true;
                break;
            case 'doku_alfamart_va' :
                $param['method_code'] = 'DOKU-ALFAMART';
                // post to API
                $apiV2 = new ApiPayment();
                $result =  $apiV2->createPayment($param);

                if (empty($result)){
                    $message = "Failed to Push Payment API";
                    break;
                }
                if ($result->response->code!=200){
                    $message = $result->response->message;
                    break;
                }
                $data = $result->data[0];

                $vaNumber = $data->virtual_account_number;
                $expiredDate = $data->datetime_expired;
                $paymentId = $data->payment_id;

                // insert payment transfer
                $paymentVADb = PaymentVirtualAccount::createPayment($paymentDb->paymentId,$paymentId,'fixed',$vaNumber,'WAITING');
                if (!$paymentVADb->isSuccess){
                    $message = 'Failed to create Payment Doku VA';
                    break;
                }
                $isSuccess = true;
                break;

            case 'otto_qr' :
                $param['method_code'] = 'OTTO-QR';
                // post to API
                $apiV2 = new ApiPayment();
                $result =  $apiV2->createPayment($param);

                if (empty($result)){
                    $message = "Failed to Push Payment API";
                    break;
                }
                if ($result->response->code!=200){
                    $message = $result->response->message;
                    break;
                }
                $data = $result->data[0];
                
                // insert payment transfer
                $paymentOttoDb = PaymentNonVA::createPayment($paymentDb->paymentId, 'qr-code',$data);
                if (!$paymentOttoDb->isSuccess){
                    $message = 'Failed to create Payment QR Ottopay';
                    break;
                }
                $isSuccess = true;
                break;

            default :
                $message = "Undefined Method Payment Function";
        }

        if (!$isSuccess){
            DB::rollback();
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();
        // create result
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        $data = $transactionDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }
}
