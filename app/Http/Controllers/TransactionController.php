<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiPopWarung;
use App\Http\Helpers\Helper;
use App\Jobs\SendFCM;
use App\Models\BackendAccessLog;
use App\Models\BalanceRecord;
use App\Models\Cart;
use App\Models\CommissionSchema;
use App\Models\Company;
use App\Models\Locker;
use App\Models\Log;
use App\Models\NotificationFCM;
use App\Models\Payment;
use App\Models\Transaction;
use App\Models\TransactionHistory;
use App\Models\TransactionItem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TransactionController extends Controller
{
    /**
     * Create Transaction from Cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTransaction(Request $request){
        
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get username
        $username = $request->input('username');
        DB::beginTransaction();
        // get cart detail
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $transactionDb = Transaction::createTransactionByCart($username,$cartDb);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // remove cart after insert to transaction
        $cartDb = Cart::removeCart($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        DB::commit();

        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$transactionDb->data]];
        return response()->json($resp);
    }

    /**
     * Create Single Transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSingleTransaction(Request $request){
        $required = ['item_name','item_type','item_price','item_param'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get variable
        $username = $request->input('username');
        $itemName = $request->input('item_name');
        $itemType = $request->input('item_type');
        $itemPrice = $request->input('item_price');
        $itemPicture = $request->input('item_picture',null);
        $itemParam = $request->input('item_param');

        DB::beginTransaction();
        // insert into cart Items
        $transactionDb = Transaction::createSingleTransaction($username,$itemName,$itemType,$itemPrice,$itemParam,$itemPicture);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();
        // generate response
        $transactionRef = $transactionDb->transactionRef;
        $transactionDb = Transaction::getTransaction($transactionRef,$username);

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$transactionDb->data]];
        return response()->json($resp);
    }

    /**
     * get single transaction detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction(Request $request){
        $required = ['transaction_ref'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get variable
        $transactionRef = $request->input('transaction_ref');
        $username = $request->input('username');
        // get transaction detail
        $transactionDb = Transaction::getTransaction($transactionRef,$username);
        if (!$transactionDb->isSuccess){
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $transactionDb->data;
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * remove transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeTransaction(Request $request){
        $required = ['transaction_ref'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get variable
        $transactionRef = $request->input('transaction_ref');
        $username = $request->input('username');
        DB::beginTransaction();
        // delete from DB
        $transDb = Transaction::removeTransaction($transactionRef,$username);
        if (!$transDb->isSuccess){
            DB::rollback();
            $message = $transDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }

    /**
     * list transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listTransaction(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $pages = $request->input('page',null);
        // get transaction list
        $transactionList = Transaction::listTransaction($username,$input,$pages);
        if (!$transactionList->isSuccess){
            $message = $transactionList->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $transactionList->data;
        if (empty($data)){
            $message = 'Empty Transaction';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $total_data = $transactionList->total_data;
        $currentPage = empty($pages) ? 1 : $pages;

        $pages = $transactionList->pages;
        $totalPages = count($pages);
        $resp=['response' => ['code' => 200,'message' =>null], 'total_data'=>$total_data, 'pages'=>$pages,'current_page'=>$currentPage,'total_pages'=>$totalPages, 'data' => $data];
        return response()->json($resp);
    }

    /**
     * CallBack Transaction Item Status
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callbackTransactionItem(Request $request){
        $response = new \stdClass();
        $response->is_success =false;
        $response->error_msg = null;

        $location = storage_path()."/logs/";
        $module = 'callbackTrx';
        Log::logFile($location,$module,"Begin Callback Transaction Item");

        $required = ['invoice_id','status','remarks','token'];
        // get all param
        $input = $request->input();
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response->error_msg = $message;
            Log::logFile($location,'callback',"Failed $message");
            return response()->json($response);
        }

        $invoiceId = $request->input('invoice_id',null);
        $status = $request->input('status',null);
        $remarks = $request->input('remarks',null);
        $token = $request->input('token',null);

        if (empty($invoiceId) || empty($status) || empty($remarks) || empty($token)){
            Log::logFile($location,$module,"Empty Invoice, Status, Remarks, Token");
            $response->error_msg = 'Empty Invoice, Status, Remarks';
            return response()->json($response);
        }

        Log::logFile($location,$module,'Callback: '.json_encode($input));

        // check token
        $checkTokenDb = Company::where('token',$token)->first();
        if (!$checkTokenDb){
            Log::logFile($location,$module,"Invalid Token");
            $response->error_msg = "Invalid Token";
            return response()->json($response);
        }

        // get transaction item based on invoice with reference
        $transactionItemDb = TransactionItem::where('reference',$invoiceId)->first();
        if (!$transactionItemDb){
            Log::logFile($location,$module,"Invoice Id not found on transaction item reference");
            $response->error_msg = 'Invoice Id not found on transaction item reference';
            return response()->json($response);
        }

        $status = strtolower($status);

        /*Process Refund*/
        DB::beginTransaction();
        $transactionId = $transactionItemDb->transaction_id;
        $userId = $transactionItemDb->transaction->users_id;
        $transactionReference = $transactionItemDb->transaction->reference;

        // if failed
        $fcmTitle = 'Transaksi';
        if ($status == 'failed'){
            $refundTransaction = $this->refundTransaction($transactionId,$remarks);
            if (!$refundTransaction->isSuccess){
                $response->error_msg = $refundTransaction->errorMsg;
                return response()->json($response);
            }
            $fcmTitle = 'Transaksi Gagal';
        } elseif ($status == 'success'){
            $this->getSerialNumber($invoiceId,$transactionItemDb->id);
            $fcmTitle = 'Transaksi Sukses';
        }


        DB::commit();

        // create parameter for FCM Notification
        $userDb = User::find($userId);
        if ($userDb){
            $fcmToken = $userDb->gcm_token;
            if (!empty($fcmToken)){
                $transactionDb = Transaction::getTransaction($transactionReference,$userDb->username);
                $data = $transactionDb->data;
                NotificationFCM::createTransactionNotification($transactionId,$fcmToken,$data,$fcmTitle,$status,'Transactions',$remarks);
            }
            dispatch(new SendFCM());
        }

        $response->is_success = true;

        Log::logFile($location,$module,"Response: ".json_encode($response));
        return response()->json($response);
    }

    private function refundTransaction($transactionId,$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $location = storage_path()."/logs/";
        $module = 'callbackTrx';

        // get transaction
        $transactionDb = Transaction::find($transactionId);
        if (!$transactionDb) {
            Log::logFile($location,$module,"Invalid Transaction");
            $response->errorMsg = "Invalid Transaction";
            return $response;
        }
        $userId = $transactionDb->users_id;
        $transactionId = $transactionDb->id;

        // check transaction status
        if ($transactionDb->status != 'PAID'){
            Log::logFile($location,$module,"Transaction status not PAID, $transactionDb->status");
            $response->errorMsg = "Transaction status not PAID, $transactionDb->status";
            return $response;
        }

        // get balance record
        $balanceRecordDb = BalanceRecord::where('transactions_id', $transactionDb->id)->first();
        if (!$balanceRecordDb) {
            Log::logFile($location,$module,"Invalid Balance Record");
            $response->errorMsg = "Invalid Balance Record";
            return $response;
        }

        // current deduct deposit
        $debit = $balanceRecordDb->debit;

        // calculate commission schema
        $transactionItemDb = $transactionDb->items;
        $totalCommission = 0;
        foreach ($transactionItemDb as $item) {
            $commissionId = $item->commission_schemas_id;
            $basicPrice = $item->price;
            $publishPrice = $item->price;
            $commissionDb = CommissionSchema::calculateById($commissionId, $basicPrice, $publishPrice);
            if ($commissionDb->isSuccess) {
                $totalCommission += $commissionDb->commission;
            }
        }

        // refund amount
        $refund = 0;
        $refund = $debit - $totalCommission;

        // create refund transaction
        $refundTransactionDb = Transaction::createRefundTransaction($userId, $remarks, $transactionDb->reference, $refund);
        if (!$refundTransactionDb->isSuccess) {
            Log::logFile($location,$module,"Failed To create Refund Transaction");
            $response->errorMsg = "Failed To create Refund Transaction";
            return $response;
        }
        // change current transaction status to refund
        $transactionDb = Transaction::find($transactionId);
        $transactionDb->status = 'REFUND';
        $transactionDb->save();
        // change payment to refund
        $paymentDb = Payment::where('transactions_id', $transactionId)->first();
        if ($paymentDb) {
            $paymentDb = Payment::find($paymentDb->id);
            $paymentDb->status = 'REFUND';
            $paymentDb->save();
        }

        // insert new transaction history
        $historyDb = TransactionHistory::createNewHistory($transactionId, 'PopBox', 'REFUND', $remarks);
        if (!$historyDb->isSuccess) {
            Log::logFile($location,$module,"Failed To create Transaction History");
            $response->errorMsg = "Failed To create Transaction History";
            return $response;
        }

        // refund commission Transaction
        $commissionTransactionDb = Transaction::refundCommissionTransaction($transactionDb->id);

        // Refund Agent Deposit
        $agentDb = User::find($userId);
        $lockerId = $agentDb->locker_id;
        $refundTransactionId = $refundTransactionDb->transactionId;
        $balanceDb = BalanceRecord::creditDeposit($lockerId, $refund, $refundTransactionId);
        if (!$balanceDb->isSuccess) {
            Log::logFile($location,$module,"Failed To create Refund Balance");
            $response->errorMsg = "Failed To create Refund Balance";
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    private function getSerialNumber($reference,$transactionItemId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $sepulsaTransactionDb = DB::connection('popbox_db')
        ->table('tb_sepulsa_transaction')
        ->leftjoin('detail_sepulsa_transaction', 'detail_sepulsa_transaction.transaction_id', '=', 'tb_sepulsa_transaction.transaction_id')
        ->select('tb_sepulsa_transaction.*', 'detail_sepulsa_transaction.serial_number')
        ->where('invoice_id',$reference)
        ->first();
        
        if (!$sepulsaTransactionDb){
            $response->errorMsg = "$reference Not Found";
            return $response;
        }
        // check status transaction
        if ($sepulsaTransactionDb->rc!='00'){
            $rc = $sepulsaTransactionDb->rc;
            $response->errorMsg = "$reference Response Code = $rc";
            return $response;
        }
        // get db
        $itemDb = TransactionItem::find($transactionItemId);
        // check if response data available
        if (empty($sepulsaTransactionDb->response_data)){
            $response->errorMsg = "$reference Response Data Empty";
            $itemDb->sn_status = 'EMPTY';
            $itemDb->save();
            $response->isSuccess = true;
            return $response;
        }
        $responseData = $sepulsaTransactionDb->response_data;
        // json decode
        $responseData = json_decode($responseData,true);
        // check sn or else depends on type
        $snStatus = null;
        $sn = null;
        $digitalProduct = ['electricity', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid', 'electricity_postpaid'];

        // if type mobile / pulsa
        if ($sepulsaTransactionDb->product_type == 'mobile'){
            if (isset($responseData['serial_number'])) {
                $snStatus = 'FOUND';
                $sn = isset($responseData['serial_number']) ? $responseData['serial_number'] : $sepulsaTransactionDb->serial_number;
            } else $snStatus = 'NOT FOUND';
        }
        
        // type tagihan
        elseif (in_array($sepulsaTransactionDb->product_type, $digitalProduct)){
            if ($sepulsaTransactionDb->product_type == 'electricity') {
                if (isset($responseData['token'])){
                    $snStatus = 'FOUND';
                    $sn = $responseData['token'];
                } else $snStatus = 'NOT FOUND';
            } elseif ($sepulsaTransactionDb->product_type != 'electricity') {
                if (isset($responseData['serial_number']) || isset($sepulsaTransactionDb->serial_number)) {
                    $snStatus = 'FOUND';
                    $sn = isset($responseData['serial_number']) ? $responseData['serial_number'] : $sepulsaTransactionDb->serial_number;
                } else $snStatus = 'NOT FOUND';
            }
        }

        $itemDb->sn_status = $snStatus;
        $itemDb->serial_number = $sn;
        $itemDb->save();

        $response->isSuccess = true;
        return $response;
    }
    /**
     * Create Transaction from Cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function voidTransaction(Request $request){
        $required = ['transaction_ref','agent_password', 'reason'];
        $message = '';
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        // get username
        $username = $request->input('username');
        $transactionRef = $request->input('transaction_ref');
        $reason = $request->input('reason');
        $agentPassword = $request->input('agent_password');
        
        $responses = $this->user_checker($username, $agentPassword);
        if(!$responses->isSuccess){
            $message = $responses->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        $params = [
            "reference"     => $transactionRef,
            "reason"	    => $reason,
            "updater_name"  => $responses->user_name,
        ];
        
        $url = config('constant.popwarung.api_url').'/transaction/voidtransactioncustomer';
        $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
        
        $api_response = json_decode($mresult, true);
        
        if(isset($api_response['payload']) && isset($api_response['payload']['response'])){
            $message = $api_response['payload']['response'];
        }
        
        $resp=['response' => ['code' => $api_response['payload']['response']['code'],'message' => $api_response['payload']['response']['message']], 'data' => [$api_response['payload']['data']]];
        return response()->json($resp);
    }
    
    public function user_checker($username, $agentPassword) {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        // get userDB
        $userDb = User::where('username',$username)->first();
        if ($userDb->status == 3){
            $response->errorMsg = 'Data Agent belum lengkap, silakan menghubungi CS Agent 0815-1897-889';
            return $response;
        }
        
        // checking hash password
        if (!Hash::check($agentPassword,$userDb->password)){
            $response->errorMsg = 'Kata sandi yang Anda masukkan belum sesuai.';
            return $response;
        }
        
        $response->user_name = $userDb->name;
        $response->isSuccess = true;
        return $response;
    }

    public function create(Request $request){
        
        $resp = ['response' => ['code' => 400, 'message' => "Failed to process request"], 'data' => []];
        
        try {
            $required = ['lockerid','memberid','regioncode','transactionref','paymenttype','totamount','customername','customeremail','customerphone','items'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp["response"]["message"] = $message;
                return response()->json($resp);
            }
            
            $locker = Locker::find($request->input("lockerid"));
            
            if(empty($locker)){
                $message = " Locker not found";
                $resp["response"]["message"] = $message;
                return response()->json($resp);
            }
            
            $insert = [
                "module" => $request->fullUrl(),
                "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
                "email" => $request->input('customeremail'),
                "name" => $request->input('customername'),
                "phone" => $request->input('customerphone'),
                "request_json" => $request->toArray(),
                "response_json" => "",
                "created_at" => date('Y-m-d H:i:s')
            ];
            
            $audit = BackendAccessLog::store($insert);
            
            $params = [];
            $params['userid']           = $request->input('memberid');
            $params['username']         = $request->input('customerphone');
            $params['lockerid']         = $locker->id;
            $params['regioncode']       = $request->input('regioncode');
            $params['transactionref']   = $request->input('transactionref');
            $params['date']             = date('Y-m-d H:i:s');
            $params['paymenttype']      = $request->input('paymenttype');
            $params['paymentid']        = $request->input('paymentid');
            $params['description']      = $request->input('description');
            $params['status']           = "PAID";
            $params['totamount']        = $request->input('totamount');
            $params['customername']     = $request->input('customername');
            $params['customeremail']    = $request->input('customeremail');
            $params['items']            = $request->input('items');
            $url = config('constant.popwarung.api_url').'/transaction/selfservice';
            $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
            
            $api_response = json_decode($mresult, true);
            
            $resp = ['response' => ['code' => $api_response['payload']['response']['code'], 'message' => $api_response['payload']['response']['message']], 'data' => []];
            
            $audit->response_json = json_encode($resp);
            $audit->updated_at = date('Y-m-d H:i:s');
            $audit->save();
            
            $resp["response"]["code"] = 200;
            $resp["response"]["message"] = "Transaction: created";
            
        } catch (\Exception $e) {
            Log::debug(get_class($e)." line #".$e->getLine().": ".$e->getMessage());
        }
        
        return response()->json($resp);
    }
    
    // Create transaction
    public function storeTransaction(Request $request){
        
        $resp = ['response' => ['code' => 400, 'message' => "Failed to process request"], 'data' => []];
        
        try {
            $required = ['lockerid','memberid','regioncode','transactionref','paymenttype','totamount','customername','customeremail','customerphone','items'];
            // get all param
            $input = $request->except('token');
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item,$input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp["response"]["message"] = $message;
                return response()->json($resp);
            }
            
            $locker = Locker::find($request->input("lockerid"));
            
            if(empty($locker)){
                $message = " Locker not found";
                $resp["response"]["message"] = $message;
                return response()->json($resp);
            }
            
            $insert = [
                "module" => $request->fullUrl(),
                "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
                "email" => $request->input('customeremail'),
                "name" => $request->input('customername'),
                "phone" => $request->input('customerphone'),
                "request_json" => $request->toArray(),
                "response_json" => "",
                "created_at" => date('Y-m-d H:i:s')
            ];
            
            BackendAccessLog::store($insert);
            
            $resp["response"]["code"] = 200;
            $resp["response"]["message"] = "Transaction: created";
            
        } catch (\Exception $e) {
            Log::debug(get_class($e)." line #".$e->getLine().": ".$e->getMessage());
        }
        
        return response()->json($resp);
    }
    
    // Submit transaction 
    public function submitTransaction(Request $request){
        $resp = ['response' => ['code' => 400, 'message' => "Failed to process request"], 'data' => []];
        
        try {
            $required = ['transactionreference','paymentreference'];
            // get all param
            $input = $request->except('token');
            
            $paramFailed = array();
            foreach ($required as $item) {
                if (!array_key_exists($item, $input)) $paramFailed[] = $item;
            }
            // if there is missing parameter
            if (!empty($paramFailed)){
                $message = "Missing Parameter : ".implode(', ',$paramFailed);
                $resp["response"]["message"] = $message;
                return response()->json($resp);
            }
            
            $insert = [
                "module" => $request->fullUrl(),
                "type" => Helper::access_from($request->header('User-Agent'), ["Chrome", "Mozilla", "Safari"]),
                "email" => NULL,
                "name" => "Payment Callback",
                "phone" => NULL,
                "request_json" => $request->toArray(),
                "response_json" => "",
                "created_at" => date('Y-m-d H:i:s')
            ];
            
            $audit = BackendAccessLog::store($insert);
            
            $backendLog = BackendAccessLog::where(["request_json->transactionref" => $request->input('transactionreference')])->select("request_json", "created_at")->orderBy("id", "DESC")->first();
            
            if(!empty($backendLog)){
                
                $date_transaction = $backendLog->created_at;
                $dataTransaction = json_decode($backendLog->request_json, true);
                
                $params = [];
                $params['userid']           = $dataTransaction["memberid"];
                $params['username']         = $dataTransaction["customerphone"];
                $params['lockerid']         = $dataTransaction["lockerid"];
                $params['regioncode']       = $dataTransaction["regioncode"];
                $params['transactionref']   = $dataTransaction["transactionref"];
                $params['date']             = "$date_transaction";
                $params['paymenttype']      = $dataTransaction["paymenttype"];
                $params['paymentid']        = $request->input('paymentreference');
                $params['description']      = $dataTransaction["description"];
                $params['status']           = "PAID";
                $params['totamount']        = $dataTransaction["totamount"];
                $params['customername']     = $dataTransaction["customername"];
                $params['customeremail']    = $dataTransaction["customeremail"];
                $params['items']            = $dataTransaction["items"];
                
                $url = config('constant.popwarung.api_url').'/transaction/selfservice';
                $mresult = ApiPopWarung::callAPI('POST', $url, json_encode($params));
                
                $api_response = json_decode($mresult, true);
                //$resp = ['response' => ['code' => $api_response['payload']['response']['code'], 'message' => $api_response['payload']['response']['message']], 'data' => []];
                
                $code = $api_response['payload']['response']['code'];
                $message = "Transaction: submitted";
                
                if($code != 200){
                    $code = 400;
                    $message = "Transaction: failed to create";
                }
                
                $audit->response_json = json_encode($api_response);
                $audit->updated_at = date('Y-m-d H:i:s');
                $audit->save();
                
                $resp["response"]["code"] = $code;
                $resp["response"]["message"] = $message;
            }
            
        } catch (\Exception $e) {
            \Illuminate\Support\Facades\Log::debug(get_class($e)." line #".$e->getLine().": ".$e->getMessage());
        }
        return response()->json($resp);
    }
}
