<?php
/**
 * User: Faisal
 */

namespace App\Http\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Api
{
    private $url = null;
    private $token;

    public function __construct() {
    }

    /**
     * @param $request
     * @param array $param
     * @return mixed
     */
    public static function callAPI($method, $url, $data) {
        Log::debug("init curl");
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));

//        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
//            'APIKEY: 111111111111111111111',
//            'Content-Type: application/json',
//        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $payload = curl_exec($curl);
        $responseInfo = curl_getinfo($curl);
        
        curl_close($curl);
        Log::debug("curl end");
        
        $result = [
            "code" => 400, 
            "message" => "failed"
        ];
        
        if($responseInfo["http_code"] == 200){
            $result["code"] = 200;
            $result["message"] = "success";
            $result["payload"] = $payload;
        }
        
        $insert = [
            'api_url' => $url,
            'api_send_data' => $data,
            'api_response'  => $payload,
            'response_date' => date("Y-m-d H:i:s")
        ];
        DB::table('companies_response')->insert($insert);
        
        return $result;
    }
}