<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 31/10/2017
 * Time: 12.16
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiVirtualLocker
{
    private $id = null;
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        $host = env('VL_URL');
        $token = env('VL_TOKEN');

        $url = $host.'/'.$request;
        $param['token'] = $token;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $user = get_current_user();
        $f = fopen(storage_path()."/logs/api/vl.$date.$user.log",'a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json'));
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path()."/logs/api/vl.$date.$user.log",'a');
        fwrite($f,$msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response'  => $output,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    /**
     * Create Parcel
     * @param $param
     * @return mixed
     */
    public function createParcel($param){
        $url = 'parcel/create';
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }
}