<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SubmitStockOpname extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            "email"                 => "required|email",
            "name"                  => "required",
            "phone"                 => "required|numeric",
            "images"                => "required",
            "images.*.filename"     => "required",
            "images.*.file"         => "required",
            "images.*.taken"        => "required",
            "warung_id"             => "required",
            "created_by"            => "required",
            "items"                 => "required",
            "items.*.product_id"    => "required",
            "items.*.product_name"  => "required",
            "items.*.max_capacity"  => "required|numeric",
            "items.*.input_stock"   => "required|numeric",
            "items.*.updated_by"    => "required"
        ];
    }
}
