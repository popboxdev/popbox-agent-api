<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ListStockOpname extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            "name"          => "required",
            "email"         => "required|email",
            "phone"         => "required|numeric",
            "start_date"    => "required|date|date_format:Y-m-d",
            "end_date"      => "required|date|date_format:Y-m-d",
            "start"         => "nullable|numeric",
            "limit"         => "nullable|numeric",
        ];
    }
    
    public function messages()
    {
        return [
            'end_date.after_or_equal'  => 'Tanggal akhir tidak boleh lebih kecil dari',
        ];
    }
}
