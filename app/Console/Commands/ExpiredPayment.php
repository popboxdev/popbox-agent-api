<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\Payment;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ExpiredPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Expired Payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get payment waiting
        echo "get today waiting payment\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'expired',"get today waiting payment");

        $startDateTime = date('Y-m-d')." 00:00:00";
        $endDateTime = date('Y-m-d H:i:s');
        $paymentTodayDb = Payment::where('status','WAITING')
            ->where('time_limit','<',$endDateTime)
            ->get();
        foreach ($paymentTodayDb as $payment){
            DB::beginTransaction();
            // check time limit
            $timeLimit = strtotime($payment->time_limit);
            $nowTime = time();
            if ($nowTime > $timeLimit){
                if (empty($payment->transaction)) {
                    echo "empty reference $payment->id\n";
                    Log::logFile($location,'expired',"empty reference $payment->id");
                    continue;
                }
                $transactionRef = $payment->transaction->reference;
                echo "$transactionRef Expired \n";
                Log::logFile($location,'expired',"$transactionRef Expired");

                // update payment and transaction to expired
                $paymentDb = Payment::find($payment->id);
                $paymentDb->status = 'EXPIRED';
                $paymentDb->save();

                $transactionDb = Transaction::find($payment->transactions_id);
                $transactionDb->status = 'EXPIRED';
                $transactionDb->save();
            }
            DB::commit();
        }
    }
}
