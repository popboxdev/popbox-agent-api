<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\TransactionItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SepulsaSerialNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:sepulsa-sn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Sepulsa Success Serial Number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->logData("Begin Get Sepulsa Success Serial Number");

        // get transaction where pushed, got reference, and sn status is empty
        $transactionItemsDb = TransactionItem::whereNotNull('reference')
            ->where('status_push', 'COMPLETED')
            ->where(function ($query){
                $query->where('sn_status','!=', 'FOUND')->orWhere('sn_status');
            })
            ->whereIn('type',['pulsa','electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid'])
            ->orderBy('id','desc')
            ->take(100)
            ->get();

        foreach ($transactionItemsDb as $item) {
            $reference = $item->reference;
            // check on popbox_db table sepulsa_transaction
            $this->logData("check ref $reference on tb_sepulsa_transaction");
            $sepulsaTransactionDb = DB::connection('popbox_db')
                ->table('tb_sepulsa_transaction')
                ->leftjoin('detail_sepulsa_transaction', 'detail_sepulsa_transaction.transaction_id', '=', 'tb_sepulsa_transaction.transaction_id')
                ->where('invoice_id', $reference)
                ->first();
            
            if (!$sepulsaTransactionDb){
                $this->logData("$reference Not Found");
                continue;
            }
            // check status transaction
            if ($sepulsaTransactionDb->rc!='00'){
                $rc = $sepulsaTransactionDb->rc;
                $this->logData("$reference Response Code = $rc");
                continue;
            }
            // get db
            $itemDb = TransactionItem::find($item->id);
            // check if response data available
            if (empty($sepulsaTransactionDb->response_data)){
                $this->logData("$reference Response Data Empty");
                $itemDb->sn_status = 'EMPTY';
                $itemDb->save();
                continue;
            }
            $responseData = $sepulsaTransactionDb->response_data;
            // json decode
            $responseData = json_decode($responseData,true);
            // check sn or else depends on type
            $snStatus = null;
            $sn = null;
            $digitalProduct = ['electricity', 'electricity_postpaid', 'bpjs_kesehatan', 'pdam', 'telkom_postpaid'];

            // if type mobile / pulsa
            if ($sepulsaTransactionDb->product_type == 'mobile'){
                if (isset($responseData['serial_number'])) {
                    $snStatus = 'FOUND';
                    $sn = $responseData['serial_number'];
                } else $snStatus = 'NOT FOUND';
            }

            // type electricity
            elseif (in_array($sepulsaTransactionDb->product_type, $digitalProduct)){
                if ($sepulsaTransactionDb->product_type == 'electricity') {
                    if (isset($responseData['token'])){
                        $snStatus = 'FOUND';
                        $sn = $responseData['token'];
                    } else $snStatus = 'NOT FOUND';
                } elseif ($sepulsaTransactionDb->product_type != 'electricity') {
                    if (isset($responseData['serial_number']) || isset($sepulsaTransactionDb->serial_number)) {
                        $snStatus = 'FOUND';
                        $sn = isset($responseData['serial_number']) ? $responseData['serial_number'] : $sepulsaTransactionDb->serial_number;
                    } else $snStatus = 'NOT FOUND';
                }
            }

            $this->logData("$reference $snStatus - $sn");
            $itemDb->sn_status = $snStatus;
            $itemDb->serial_number = $sn;
            $itemDb->save();
            $this->logData("$reference updated");
        }
    }

    /**
     * Log Data
     * @param string $message
     */
    private function logData($message=''){
        echo "$message\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'sepulsaSN',$message);
        return;
    }
}
