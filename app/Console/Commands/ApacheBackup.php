<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;


class ApacheBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apachebackup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'backup and reset apache log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    
    public function handle()
    {

        // /data/developer/popbox-agent-api/storage/logs/access

		// tentukan file backup yang usianya 3 hari
    	$tanggal = date('Y-m-d', strtotime('-3 days'));
    	$date = date('Y.m.d',strtotime('-3 days'));
      	$backupfile = "access_".$tanggal.".log";



    	// backup log popagent-api for apache access 
   		exec("cp /var/log/apache2/agent/access.log /data/developer/popbox-agent-api/storage/logs/apache/");
   		exec("mv /data/developer/popbox-agent-api/storage/logs/apache/access.log /data/developer/popbox-agent-api/storage/logs/apache/access_`date '+%Y-%m-%d'`.log");
   		exec("> /var/log/apache2/agent/access.log"); 
      	if (file_exists("/data/developer/popbox-agent-api/storage/logs/apache/$backupfile")) {
         	exec("rm /data/developer/popbox-agent-api/storage/logs/apache/$backupfile");
      	}
         
       	// backup log apache virtualocker 	
       	exec("cp /var/log/apache2/virtualocker/access.log /data/developer/virtualocker/storage/logs/apache/");
   		exec("mv /data/developer/virtualocker/storage/logs/apache/access.log /data/developer/virtualocker/storage/logs/apache/access_`date '+%Y-%m-%d'`.log");
   		exec("> /var/log/apache2/virtualocker/access.log");    
      	if (file_exists("/data/developer/virtualocker/storage/logs/apache/$backupfile")) {
         	exec("rm /data/developer/virtualocker/storage/logs/apache/$backupfile");
      	}   
      	
      	// backup apache setting
      	exec("cp /etc/apache2/sites-enabled/* /data/developer/apacheconfig/");

        /*Remove that file old 3 days*/
        // file logging for popagent-api unknown access log
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/access/$date.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/access/$date.log");
        }

        // Database agent api
        if (file_exists("/data/developer/popbox-agent-api/storage/dbackup/popbox_agent_db_$tanggal.sql.gz")) {
            exec("rm /data/developer/popbox-agent-api/storage/dbackup/popbox_agent_db_$tanggal.sql.gz");
        }
        // Database virtual locker
        if (file_exists("/data/developer/virtualocker/storage/dbackup/virtualocker_db_$tanggal.sql.gz")) {
            exec("rm /data/developer/virtualocker/storage/dbackup/virtualocker_db_$tanggal.sql.gz");
        }

        // callback
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/callback.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/callback.$date-developer.log");
        }
        // callback transaction
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/callbackTrx.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/callbackTrx.$date-developer.log");
        }
        // commission
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/commission.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/commission.$date-developer.log");
        }

        // cron folder
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/expired.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/expired.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/lockerNameReferral.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/lockerNameReferral.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/lockerVAOpen.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/lockerVAOpen.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/referralTransaction.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/referralTransaction.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/sepulsaSN.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/sepulsaSN.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/transaction_report.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/transaction_report.$date-developer.log");
        }
        if (file_exists("/data/developer/popbox-agent-api/storage/logs/cron/transactionPending.$date-developer.log")) {
            exec("rm /data/developer/popbox-agent-api/storage/logs/cron/transactionPending.$date-developer.log");
        }
        if (file_exists("/data/developer/virtualocker/storage/logs/cron/lockerDistrictData$date.log")) {
            exec("rm /data/developer/virtualocker/storage/logs/cron/lockerDistrictData$date.log");
        }
        if (file_exists("/data/developer/virtualocker/storage/logs/cron/PopBoxLockerStatus.$date.log")) {
            exec("rm /data/developer/virtualocker/storage/logs/cron/PopBoxLockerStatus.$date.log");
        }
    }
}
