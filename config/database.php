<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_AGENT_HOST', 'localhost'),
            'port' => env('DB_AGENT_PORT', '3306'),
            'database' => env('DB_AGENT_DATABASE'),
            'username' => env('DB_AGENT_USERNAME'),
            'password' => env('DB_AGENT_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'popbox_db' => [
            'driver' => 'mysql',
            'host' => env('DB_POPBOX_HOST'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_POPBOX_DATABASE'),
            'username' => env('DB_POPBOX_USERNAME'),
            'password' => env('DB_POPBOX_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],

        'popbox_virtual' => [
            'driver' => 'mysql',
            'host' => env('DB_LOCKER_HOST'),
            'port' => env('DB_LOCKER_PORT', '3306'),
            'database' => env('DB_LOCKER_DATABASE'),
            'username' => env('DB_LOCKER_USERNAME'),
            'password' => env('DB_LOCKER_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'popbox_express' => [
            'driver' => 'mysql',
            'host' => env('DB_EXPRESS_HOST'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_EXPRESS_DATABASE'),
            'username' => env('DB_EXPRESS_USERNAME'),
            'password' => env('DB_EXPRESS_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
