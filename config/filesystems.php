<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],
        
        'public_path' => [
            'driver' => 'local',
            'root'   => public_path() . '/uploads',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'upload' => [
            'driver' => 'local',
            'root'   => public_path() . '/upload',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'banner' => [
            'driver' => 'local',
            'root'   => public_path() . '/banner',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'idPict' => [
            'driver' => 'local',
            'root'   => public_path() . '/agent/id',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'selfiePict' => [
            'driver' => 'local',
            'root'   => public_path() . '/agent/id_selfie',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'storePict' => [
            'driver' => 'local',
            'root'   => public_path() . '/agent/store',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'npwpPict' => [
            'driver' => 'local',
            'root'   => public_path() . '/agent/npwp',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],
        
        'stockopname' => [
            'driver' => 'local',
            'root'   => public_path() . '/stockopname',
            'url' => env('APP_URL').'/public',
            'visibility' => 'public',
        ],

    ],

];
