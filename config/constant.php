<?php
/**
 * User: faisal
 * Date: 11/12/2018
 * Time: 14.34
 */

return [
    'popwarung' => [
        'api_url' => env('WARUNG_API_URL'),
    ],
    'report' => [
        'url' => env('REPORT_URL', '<base>.popbox.asia'),
    ],
    'locker' => [
        'open' => env('OPEN_LOCKER', "open-warung"),
        'status' => env('LOCKER_STATUS', "get-status-door"),
    ],
    'internal' => [ 
        "79XM983RH8TK37KTR84NNUCYK8DTLTRDXE5Z77UP", 
        "UQFEGPJALHTNKNWW3S3BCVR2RDF38WAR5XACGKD9", 
        "L45KLW6QBNVWYQ7OX2JBRBADML3AQHB7C32EYN49"
    ],
    "agent_url" => env("AGENT_URL"),
    "rasi"  => [
        "baseurl"   => env("RASI_URL", "https://wms-api-demo.rasi.app"),
        "endpoint"  => [
            "login"         => env("RASI_LOGIN_ENDPOINT", "/api/v2/auth/login"),
            "api_key"       => env("API_KEY", "iKZUz5g1PedZQQM3SNCcyRY01aUhIk7fdCcC35s4iZOoJGM2V8kYbPnLs52dYH"),
            "api_secret"    => env("API_SECRET", "mexIjEV6E8BQqkrmNd4mr636klTIUmZxx0nxJbVZyuGokwkHdy2J42MpJviuP9"),
            "create"        => [
                "delivery_order"    => env("DELIVERY_ORDER"),
                "outbound_order"    => env("OUTBOUND_ORDER"),
                "inbound_order"     => env("INBOUND_ORDER"),
            ],
        ],
        "origin_id"         => env("ORIGIN_ID"),
        "transporter_id"    => env("TRANSPORTER_ID"),
    ],
];