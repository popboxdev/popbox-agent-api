<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovalColumnToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->string('approved_by',128)->nullable()->after('priority')->comment('filled by user name of approval');
            $table->string('approval_status',128)->default('pending')->after('approved_by')->comment('Approval Status: Active, Pending,Rejected');
        });
        Schema::table('commission_schemas', function (Blueprint $table) {
            $table->string('approved_by',128)->nullable()->after('end_date')->comment('filled by user name of approval');
            $table->string('approval_status',128)->default('pending')->after('approved_by')->comment('Approval Status Active, Pending,Rejected');
        });
        Schema::table('referral_campaigns', function (Blueprint $table) {
            $table->string('approved_by',128)->nullable()->after('expired')->comment('filled by user name of approval');
            $table->string('approval_status',128)->default('pending')->after('approved_by')->comment('Approval Status Active, Pending,Rejected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('approved_by');
            $table->dropColumn('approval_status');
        });
        Schema::table('commission_schemas', function (Blueprint $table) {
            $table->dropColumn('approved_by');
            $table->dropColumn('approval_status');
        });
        Schema::table('referral_campaigns', function (Blueprint $table) {
            $table->dropColumn('approved_by');
            $table->dropColumn('approval_status');
        });
    }
}
