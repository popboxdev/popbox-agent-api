<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locker_id',100);
            $table->date('pickup_date');
            $table->string('pickup_time',100);
            $table->text('pickup_location');

            $table->string('awb_number',255);
            $table->string('sender_phone',15);
            $table->string('sender_name',255);
            $table->string('sender_email',255)->nullable();

            $table->string('recipient_name',255);
            $table->string('recipient_phone',15);
            $table->text('recipient_address');
            $table->string('recipient_email',255)->nullable();
            $table->string('destination_district_code',50);

            $table->string('reseller',255)->nullable();
            $table->string('service_type',100);
            $table->integer('weight');
            $table->float('actual_weight');

            $table->string('status',100);

            $table->integer('price_per_kg');
            $table->bigInteger('parcel_price');
            $table->integer('insurance')->default(0);
            $table->bigInteger('total_price');

            $table->string('pickup_number',255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_orders');
    }
}
