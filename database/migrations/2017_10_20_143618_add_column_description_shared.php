<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDescriptionShared extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referral_campaigns', function (Blueprint $table) {
            $table->string('description_to_shared',500)->nullable()->after('description');
            $table->text('rule')->nullable()->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referral_campaigns', function (Blueprint $table) {
            $table->dropColumn('description_to_shared');
            $table->dropColumn('rule');
        });
    }
}
