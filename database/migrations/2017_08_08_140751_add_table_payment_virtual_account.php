<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePaymentVirtualAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_virtual_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payments_id');
            $table->string('payment_id',100)->nullable();
            $table->string('va_type',100)->nullable();
            $table->string('va_number',150)->nullable();
            $table->string('status',100)->default('WAITING');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payments_id')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_virtual_accounts');
    }
}
