<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/',function (){
   return 'ok';
});

Route::group(['prefix'=>'agent'],function (){
    Route::post('register','UserController@agentRegister');
    Route::post('update','UserController@agentUpdate');
    Route::post('login','UserController@agentLogin');
    Route::post('logout','UserController@agentLogout');
    Route::post('addLocker','VirtualLockerController@registerVirtualLocker');
    Route::post('check','UserController@agentCheck');
    Route::post('checkpassword','UserController@checkPassword');
    Route::post('forgot','UserController@forgotPassword');
    Route::post('reset','UserController@resetPassword');
    Route::post('activate','UserController@agentActivation');
    Route::post('ping','UserController@getUserPing');
    Route::post('changePassword','UserController@changePassword');
    Route::post('checkGcmToken','UserController@checkGcmToken');
    Route::post('profile','UserController@getAgentProfile');
    Route::post('referral','UserController@getReferralProfile');
    Route::post('completeProfile','UserController@postCompleteProfile');
});

Route::group(['prefix'=>'referral'],function(){
   Route::post('check','ReferralController@checkReferral');
});

Route::group(['prefix'=>'cart'],function(){
    Route::post('check','CartController@checkCart');
    Route::post('checkpurchase','CartController@checkPurchase');
    Route::post('insertItem','CartController@insertItem');
    Route::post('update-item','CartController@updateItem');
    Route::post('update-items','CartController@updateItems');
    Route::post('removeItem','CartController@removeItem');
});

Route::group(['prefix'=>'transaction'],function(){
    Route::post('get','TransactionController@getTransaction');
    Route::post('list','TransactionController@listTransaction');
    Route::post('create','TransactionController@createTransaction');
    Route::post('createSingle','TransactionController@createSingleTransaction');
    Route::post('remove','TransactionController@removeTransaction'); # only for unpaid status
    Route::post('void','TransactionController@voidTransaction');
});

Route::group(['prefix'=>'payment'],function(){
    Route::post('getMethod','PaymentController@getMethod');
    Route::post('addPayment','PaymentController@createPayment');
    Route::post('paid','PaymentController@paidPayment');
    Route::post('detail','PaymentController@detailPayment');
    Route::post('createPaidTransaction','PaymentController@createPaidTransaction');
});

Route::group(['prefix'=>'topup'],function(){
    Route::post('getTopupMethod','TopUpController@getTopUpMethod');
    Route::post('getBankDestination','TopUpController@getBankDestination');
    Route::post('createBankTransfer','TopUpController@createBankTransfer');
    Route::post('confirmBankTransfer','TopUpController@confirmBankTransfer');
    Route::post('createDokuVA','TopUpController@createDokuVATransfer');
    Route::post('createVirtualAccount','TopUpController@createVirtualAccount');
});

Route::group(['prefix'=>'locker'],function(){
   Route::post('getlocation','LockerController@getLockerLocation');
   Route::post('detail','LockerController@getLocker');
});

Route::group(['prefix' => 'balance'],function (){
   Route::post('list','BalanceController@balanceRecord');
   Route::post('creditDeposit','BalanceController@creditDeposit');
});

Route::group(['prefix'=>'service'],function (){
    Route::post('getOperatorPulsaProduct','ServiceController@getPulsaOperator');
    Route::post('getPulsaProduct','ServiceController@getPulsaProduct');
    Route::post('getPLNPrePaidProduct','ServiceController@getPLNPrePaidProduct');
    Route::post('getPLNPrePaidInquiry','ServiceController@getPLNPrePaidInquiry');
    Route::post('postPLNPrePaid','ServiceController@postPLNPrePaid');
    Route::post('getPLNPostPaidInquiry','ServiceController@getPLNPostPaidInquiry');
    Route::post('postPLNPostPaid','ServiceController@postPLNPostPaid');
    Route::post('getBPJSInquiry','ServiceController@getBPJSInquiry');
    Route::post('postBPJS','ServiceController@postBPJS');
    Route::post('getPDAMProduct','ServiceController@getPDAMOperator');
    Route::post('getPDAMInquiry','ServiceController@getPDAMInquiry');
    Route::post('postPDAM','ServiceController@postPDAM');
    Route::post('getTelkomInquiry','ServiceController@getTelkomInquiry');
    Route::post('postTelkom','ServiceController@postTelkom');
    Route::post('getPopShopProduct','ServiceController@getPopShopProduct');
    Route::post('getPopShopCategory','ServiceController@getPopShopCategory');
    
    Route::group(['prefix' => 'message'], function () {
        Route::post('store-file','ServiceController@postStoreFile')->name('store-file');
        Route::post('send','ServiceController@postSendMessage')->name('send-message');
        Route::post('list','ServiceController@getMessage')->name('list-message');
        Route::post('update','ServiceController@updateMessage')->name('update-message');
    });
});

Route::group(['prefix' => 'banner'],function (){
    Route::post('list','BannerController@list');
});

Route::group(['prefix'=>'parcel'],function (){
    Route::post('getCity','ParcelController@getCity');
    Route::post('orderCalculation','ParcelController@getOrderCalculation');
    Route::post('orderSubmit','ParcelController@postOrderSubmit');
});

Route::group(['prefix'=>'delivery'],function(){
    Route::post('getCityOrigin','DeliveryController@getCityOrigin');
    Route::post('getProvinceDestination','DeliveryController@getProvinceDestination');
    Route::post('getCityDestination','DeliveryController@getCityDestination');
    Route::post('getProvinceDestination','DeliveryController@getProvinceDestination');
    Route::post('getDestination','DeliveryController@getDestination');
    Route::post('getCheckTariff','DeliveryController@getCheckTariff');
    Route::post('getCalculatePickup','DeliveryController@getCalculatePickup');
    Route::post('postPickupOrder','DeliveryController@postSubmitPickup');
    Route::post('getHistories','DeliveryController@getHistories');
});
    
    Route::group(['prefix' => 'locker'], function(){
        Route::post('open-door', 'LockerController@open')->name('open-locker-door');
    });
        
    Route::group(['prefix' => 'cabinet'], function(){
        Route::any('open', 'CabinetController@open')->name('open-cabinet');
        Route::post('transaction/create', 'TransactionController@create')->name('create-transaction');
        Route::post('transaction/list', 'CabinetController@list')->name('list-transaction');
        Route::post('product/findbyparams', 'CabinetController@findByParams')->name('find-by-params');
        Route::post('transaction/summary', 'CabinetController@summary')->name('summary-trx');
        Route::post('transaction/store', 'TransactionController@storeTransaction')->name('store-transaction');
        Route::post('transaction/submit', 'TransactionController@submitTransaction')->name('submit-transaction');
        Route::post('transaction/submit', 'TransactionController@submitTransaction')->name('submit-transaction');
        Route::group(['prefix' => 'stock-opname'], function(){
            Route::post('list', 'CabinetController@listStockOpname')->name('stock-opname-list');
            Route::post('submit', 'CabinetController@submitStockOpname')->name('submit-stock-opname');
        });
    });

/*Legacy replaced with articles*/
Route::group(['prefix'=>'news'],function (){
    Route::post('getNews','NewsController@getNews');
});

Route::group(['prefix'=>'articles'],function (){
    Route::post('list','ArticleController@getArticles');
});

Route::group(['prefix'=>'version'],function(){
    Route::post('getlastversion','VersionController@getLastVersion');
});

Route::group(['prefix'=>'receipt'],function(){
    Route::post('getReceiptByTransaction','ReceiptController@getReceiptByTransaction');
    Route::post('updateReceiptByBatch','ReceiptController@updateReceiptByBatch');
});